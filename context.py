import yaml
from yaml import YAMLObject
from ui.interface import Command, command_list, FileArgument, StringArgument
from effects import EffectList



class Context(YAMLObject):
    yaml_tag = 'context'
    
    fader = 1
    other_fader_idx = 2
    time = 0
    rescore = False
    shortcuts = [None for i in range(11)]
    enable_faders = True
    solo = None
    enable_global_effects = True
    metronome = 0
    preview = None
    preview_playing = False
    headphone_mix = 1.0
    current_headphone_volume = 1.0
    crossfade_time = 0.46
    current = None
    play_list = []
    play_list_index = 0
    effect_sliders = {}
    global_loop = None

    

    def all_effects(self):
        yield from self.effects
        for t in self.all_tracks:
            yield from t.effects


    def find_by_output(context, output):
        found = None
        for t in (context.all_tracks  + [context.preview]):
            if t.output == output:
                found = t
                break
        if found is None:
            raise IndexError("Output {} not found".format(output))
        return found

    def place(self, track, t, cue):
        track.start = t
        track.jump_point = cue
        for i, v in enumerate(self.shortcuts):
            if v is track:
                track.fades.initial = context.initial_fades[i]

        if track not in self.all_tracks:
            self.all_tracks.append(track)
        if self.preview is track:
            self.preview = None
        track.is_placed = True
        self.mark_dirty(track)
        

    def mark_current_dirty(self, *, restart_preview = False, rescore = True):
        return self.mark_dirty(self.current, restart_preview = restart_preview,
                               rescore = rescore)

    def mark_dirty(self, track, *, restart_preview = False,
                   rescore = True):
        from csound import csound_interaction
        if restart_preview and context.preview:
            context.preview.start = context.time
            context.preview.is_placed = False
        if csound_interaction:
            if (track is self.preview) and not self.preview_playing:
                csound_interaction.silence_track(track)
            else:
                csound_interaction.score_track(track)
                csound_interaction.score_globals()
        else: self.rescore = rescore

    def play_excerpt(self, track, pos):
        from csound import csound_interaction
        if csound_interaction:
            csound_interaction.play_excerpt(track, pos)
    

    def mark_globals_dirty(self):
        from csound import csound_interaction
        if self.current is None: return
        if csound_interaction:
            csound_interaction.score_globals()
        else:
            self.rescore = True

    def mark_global_effects_dirty(self, rescore = True):
        from csound import csound_interaction
        if csound_interaction:
            csound_interaction.score_global_effects()
        else: self.rescore = rescore
        

    def load(self, f):
        with open(f,'rt') as f:
            context2 = yaml.load(f.read())
            d = context2
            if isinstance(d, Context):
                d = d.__dict__
            for k,v in d.items():
                setattr(self, k, v)
            self.global_loop = None
        for t in self.shortcuts:
            if t is None: continue
            if t.start is not None and t not in self.all_tracks:
                self.all_tracks.append(t)
        try:
            self.current_effect.context = self
        except: pass

        self.now = None
        self.preview = None

    def mark_silent(self, track):
        from csound import csound_interaction
        if csound_interaction:
            csound_interaction.silence_track(track)

    def update_effect_sliders(self, sbank, effect, output):
        try:
            old_output = self.effect_sliders[sbank][1]
        except KeyError:
            old_output = None
        self.effect_sliders[sbank] = (effect, output)
        if output == old_output: return
        if old_output is None: return
        if old_output == 0: self.mark_global_effects_dirty()
        else:
            track = self.find_by_output(old_output)
            self.mark_dirty(track, rescore = False)
            

context = Context()
context.shortcuts = context.shortcuts
context.initial_fades = [0, 127]+[0 for i in range(9)]
context.all_tracks = []
context.effects = EffectList(0)
context.play_list = []





__all__ = ['context']
