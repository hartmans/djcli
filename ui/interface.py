import os, os.path, re
from abc import ABCMeta, abstractmethod, abstractclassmethod
from inspect import isawaitable
from prompt_toolkit.document import Document
from prompt_toolkit.completion import Completion, Completer
from .time import Resolvable


class Command(metaclass = ABCMeta):

    def __init__(self):
        if isinstance(self.arguments,type):
            self.arguments = self.arguments()

    @abstractclassmethod
    def name(self):
        raise NotImplementedError()

    arguments = None
    aliases = tuple()

    @abstractmethod
    def execute(self, argument_instances, context):
        raise NotImplementedError

    def complete(self, document, complete_event):
        text = document.text_before_cursor
        command, space, args = text.partition(' ')
        if not command: return
        args_document = Document(args)
        yield from self.arguments.complete(args_document, complete_event)


class CommandList:

    def __init__(self):
        self.commands = {}

    def add_command(self, cmd):
        if isinstance(cmd,type) and issubclass(cmd,Command):
            c = cmd()
        else:
            c = cmd
            assert isinstance(c, Command)
        self.commands[c.name] = c
        for alias in c.aliases:
            self.commands[alias] = c
        return c

    def complete(self, document, event):
        text = document.text_before_cursor
        if " " not in text:
            if not event.completion_requested: return
        command, sep, tail = text.partition(" ")
        if not tail:
            for n, c in sorted(self.commands.items()):
                if not n.startswith(command): continue
                prefix = os.path.commonprefix([c.name, command])
                yield Completion(c.name, len(prefix)-len(c.name), n)
        else: #tail
            if command not in self.commands: return
            yield from self.commands[command].complete(Document(text), event)

    async def execute(self, text, context):
        command, sep, tail = text.partition(" ")
        if not command: return
        try:
            c = self.commands[command]
        except KeyError:
            raise SyntaxError("Illegal command") from None
        if not c.arguments and tail:
            raise SyntaxError("Arguments not taken by {}".format(command)) from None
        if c.arguments is not None:
            try: args = c.arguments.parse(tail, context = context)
            except TypeError: args = c.arguments.parse(tail)
        else: args = None
        if isawaitable(args): args = await args
        if isinstance(args,Resolvable):
            args = args.resolve(context)
        res = c.execute(args, context)
        if isawaitable(res):
            res = await res
        return res


class CompletionHelper(metaclass = ABCMeta):

    @abstractmethod
    def completion_tuples(self):
        raise NotImplementedError

    def __init__(self, min_length, *args, **kwargs):
        self.min_length = min_length
        
    def complete(self, document, complete_event):
        text = document.text_before_cursor
        if len(text) < self.min_length and not complete_event.completion_requested: return
        match_re = self.match_re(text)
        for match, complete, display in self.completion_tuples():
            if match_re.search( match) is None: continue
            prefix = os.path.commonprefix([text, complete])
            yield Completion(complete, len(prefix)-len(text), display)

    @abstractmethod
    def match_re(self, text):
        raise NotImplementedError

class FileArgument(CompletionHelper):
        

    def __init__(self, dirs, min_length = 6):
        super().__init__(min_length = min_length)
        self.dirs = dirs
        self.files = []


    def parse(self, tail, context):
        return tail

    def completion_tuples(self):
        if not self.files:
            for d in self.dirs:
                for f in os.listdir(d):
                    self.files.append(file_completion_tuple(d,f))
            self.files = sorted(self.files, key = lambda e: e[1])
        return self.files

    def match_re(self, match_text):
        match_text = re.escape(match_text.lower())
        match_text = re.sub(r'\\ ', '.+', match_text)
        return re.compile(match_text)
    

class NumberArgument:

    def __init__(self): pass

    def complete(self, document, event):
        if False: yield
        return


    def parse(self, tail, context):
        if tail == "": return None
        if tail is None: return None
        return float(tail)


class  StringArgument:

    def complete(self, document, event):
        if False: yield
        return

    def parse(self, arg, context):
        return arg

class TimeArgument(NumberArgument):

    def parse(self, tail, context):
        if tail is None: return None
        if tail == "": return None
        if tail[0] == '@':
            from .time import CuePointMarker
            return CuePointMarker(int(tail[1:]))
        else:
            return super().parse(tail, context = context)
        


class OffsetArgument(NumberArgument):

    def parse(self, tail, context):
        if tail is None: return None
        if tail == "": return None
        if tail[0] == '@':
            from .time import CuePointMarker
            return CuePointMarker(int(tail[1:]), offset = True)
        elif tail.startswith('t:') or tail.startswith('t='):
            from .time import TimeMarker
            return TimeMarker(float(tail[2:]))
        else:
            return super().parse(tail, context = context)
        



file_match_table = {
    '-': None,
    '_': None,
    ' ': None,
    '!': None
}
def file_match_text(file):
    return file.lower().translate(file_match_table)

def file_completion_tuple(d, f):
    joined = os.path.join(d,f)
    return (file_match_text(joined), joined, f)

command_list = CommandList()



class OtherTrackArgument:

    def parse(self, tail, context):
        if tail in 'abcdefghij':
            fader = ord(tail)-ord('a')+1
            return context.shortcuts[fader]
        if tail[0] == 'o':
            output = int(tail[1:])
            for t in context.all_tracks:
                if t.output == output: return t
        return None

    def complete(self, *args):
        return []
    
        
class CommandListCompleter(Completer):

    def get_completions(self, document, event):
        return command_list.complete(document, event)

class ListArgument:

    def __init__(self, *args):
        if len(args) == 1:
            args = args[0]
        args = list(args)
        self.defaults = [None]*len(args)
        for i in range(len(args)):
            if isinstance(args[i], (tuple, list)):
                self.defaults[i] = args[i][1]
                args[i] = args[i][0]
            if isinstance(args[i], type):
                args[i] = args[i]()
        self.args = args

    def parse(self, instance, context):
        values = re.split('\s+', instance, len(self.args)-1)
        if values and values[-1] is None:
            values.pop()
        if len(values) < len(self.args):
            values += self.defaults[(len(values)):]
        args =  list(map(lambda a,v: a.parse(v, context = context), self.args, values))
        for i, a in enumerate(args):
            if isinstance(a, Resolvable):
                args[i] = a.resolve(context)
        return args

    def complete(self, document, complete_event):
        values = re.split('\s+', document.text_before_cursor, len(self.args)-1)
        target_arg = self.args[len(values)-1]
        arg_document = Document(values[-1])
        yield from target_arg.complete(arg_document, complete_event)
        
__all__ = ['Command', 'FileArgument',
           'NumberArgument',
           'ListArgument',
           'StringArgument',
           'TimeArgument',
           'CommandListCompleter',
           'command_list',
           "OtherTrackArgument",
           'OffsetArgument']

