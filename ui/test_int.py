from interface import *
from prompt_toolkit import prompt
from prompt_toolkit.completion import Completer

class FileCompleter(Completer):

    def get_completions(self, document, event):
        f = FileArgument(("/home/hartmans/Music", ))
        return f.complete(document, event)

while True:
    print(prompt(">", completer=FileCompleter(), complete_while_typing = True))
    
