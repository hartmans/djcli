
class Resolvable:

    __slots__ = tuple()
    
    
class CuePointMarker(Resolvable):

    __slots__ = ('cue_point', 'offset')

    def __init__(self, cue_point, offset = False):
        self.cue_point = cue_point
        self.offset = offset

    def resolve(self, context):
        if self.offset:
            return context.current.cue_point_offset(self.cue_point)
        return context.current.cue_point_time(self.cue_point)
    

class TimeMarker(Resolvable):

    __slots__ = ('t',)

    def __init__(self, t):
        self.t = t

    def resolve(self, context):
        return context.current.offset_at(self.t)
    
