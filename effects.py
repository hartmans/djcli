#!/usr/bin/python3
import weakref
from yaml import YAMLObject
from fader import FaderList

SLIDERS = 3
FADERS=10
effects = weakref.WeakValueDictionary()

class Effect:

    def __new__(cls, name, *args, **kwargs):
        if name in effects: return effects[name]
        return super().__new__(cls, *args, **kwargs)
    
    def __init__(self, name):
        self.slider_base = None
        self.fader_instr = None
        self.global_instr = None
        self.name = name
        assert self.name not in effects or effects[self.name] is self
        effects[self.name] = self

    @property
    def code(self):
        with open("effects/"+self.name, "rt", encoding = 'utf-8') as f:
            return f.read()

    def zk(self, fader, slider_num):
        "Return the zk variable to use for slider_num in fader set fader.  Fader set 0 is the global output chain"
        if self.slider_base is None:
            raise RuntimeError('Effect {} is not yet numbered'.format(self.name))
        slider_num -= 1 #0 based in zk space
        return self.slider_base+SLIDERS*fader+slider_num

            

    @staticmethod
    def number_effects():
        base = 20
        fader_instr = 31
        global_instr = 61
        for e in effects.values():
            e.slider_base = base
            e.fader_instr = fader_instr
            e.global_instr = global_instr
            global_instr += 1
            fader_instr += 1
            base += SLIDERS*(FADERS+1)
        return base

class EffectEntry:

    __slots__ = ('effect', 'start', 'end', 'args')

    def __init__(self, effect, start, end, args):
        assert end > start
        assert start >= 0
        self.effect = effect
        self.start = start
        self.end = end
        self.args = args

    def __contains__(self, other):
        "Is time t in the range of this effect"
        if not isinstance(other, (int, float)):
            return NotImplemented
        return self.start <= other < self.end

    def overlap(self, other):
        assert isinstance(other, EffectEntry)
        return (other.start in self) or (other.end-0.001 in self) \
            or (self.start in other) or (self.end-0.001 in other)


    def __format__(self, spec):
        return "{}( from {t1} to {t2})".format(
            self.effect.name, t1 = self.start, t2 = self.end)


class ZkNumber(YAMLObject):
    yaml_tag = "zknumber"
    
    __slots__ = ('elist', 'effect', 'slider')

    def __init__(self, elist, effect, slider):
        self.elist = weakref.ref(elist)
        self.effect = effect
        self.slider = slider

    def __getstate__(self):
        return {
            'e': self.effect.name,
            's': self.slider,
            }

    
    def __setstate__(self, state):
        if state['e'] in effects:
            self.effect = effects[state['e']]
        else: self.effect = Effect(state['e'])
        self.slider = state['s']

        
    def __int__(self):
        return self.effect.zk(self.elist().fader, self.slider)

            
    def __format__(self, spec):
        return str(self.__int__())

    
class EffectList(YAMLObject):

    yaml_tag = 'effect_list'
    

    def __init__(self, fader):
        self.fader = fader
        self.list = []
        self.sliders= {}
        

    def add_effect(self, name, start, end, args):
        from csound import CSOUND_LIVE_INFINITY
        if args is None: args = ""
        if name in effects: effect = effects[name]
        else: effect = Effect(name)
        entry = EffectEntry(effect, start, end, args)
        for e in self.list:
            if e.effect is not effect: continue
            if e.end == CSOUND_LIVE_INFINITY:
                print("Ending previous live effect")
                e.end = entry.start

            if entry.overlap(e):
                raise RuntimeError("{} and {} overlap".format(entry, e))
        self.list.append(entry)
        self.list.sort(key = lambda e: (e.start, e.effect.name, e.end))

    def remove_effects(self, name, from_, to):
        if name not in effects:
            raise RuntimeError("Effect not loaded")
        effect = effects[name]
        def filter_func(e):
            if  effect is not e.effect: return True
            if from_ <= e.start and e.start <= to: return False
            if from_ <= e.end and e.end <= to: return False
            return True
        self.list = list(filter(filter_func, self.list))

    def __getitem__(self, name):
        if name not in effects:
            raise KeyError("Effect not loaded")
        if name not in self.sliders:
            effect = effects[name]
            self.sliders[name] = [ FaderList(ZkNumber(self, effect, s), initial = 0)
                                   for  s in range(1, SLIDERS+1)]
        return [None] + self.sliders[name]
    def end_live(self, effect, t):
        from csound import CSOUND_LIVE_INFINITY
        for e in self.list:
            if e.effect.name == effect and e.end == CSOUND_LIVE_INFINITY:
                e.end = t
                return
        raise IndexError("Live Effect {} not found".format(effect))
    

    def __getstate__(self):
        effects_used = frozenset(map( lambda e: e.effect.name, self.list))
        for k in list(self.sliders.keys()):
            if k not in effects_used:
                del self.sliders[k]
        return {
            'fader': self.fader,
            'list': [[e.effect.name, e.start, e.end, e.args] for e in self.list],
            'sliders': self.sliders}

    def after_time(self, offset):
        for e in self.list:
            if e.end < offset: continue
            yield e.start, e, e.end-e.start
            
        
    def __setstate__(self, state):
        l = state.pop('list')
        self.list = []
        for e in l:
            self.add_effect(e[0], e[1], e[2], e[3])
        self.__dict__.update(state)
        for sl in self.sliders.values():
            for l in sl:
                l.fader.elist = weakref.ref(self)

    def __iter__(self):
        for e in self.list:
            yield e.effect

    def pitch_scale(self, pitch):
        "Pitch scale in place for format conversion"
        for ee in self.list:
            ee.start = ee.start*pitch
            ee.end = ee.end*pitch
        for k,v in self.sliders.items():
            self.sliders[k] = [x.pitch_scale(pitch) for x in v]
        return self
    
        
__all__ = ['Effect', 'EffectList']
