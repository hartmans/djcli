from ui.interface import *
from effects import *
import effects
from fader import FaderSegment

class CurrentFaderEffect:

    def __init__(self, effect, context):
        self.context = context
        self.effect = effect

    def add(self, t2, args):
        self.context.current.add_effect(self.effect,
                                        self.context.time, t2, args)

    def add_live(self, sbank, args):
        self.context.update_effect_sliders(sbank, self.effect, self.context.current.output)
        return self.add("infinity", args)
    
        
    def slide(self, slider, val, dur):
        self.context.current.add_slide(self.effect,
                                       slider, self.context.time,
                                       val, dur)
        self.context.mark_current_dirty()

    def remove(self, t2):
        self.context.current.remove_effect(self.effect, self.context.time,
                                           t2)
        self.context.mark_current_dirty()

    def remove_slides(self, slider, t2):
        self.context.current.remove_slides(self.effect, slider,
                                           self.context.time, t2)
        self.context.mark_current_dirty()

class CurrentGlobalEffect:

    def __init__(self, effect, context):
        self.context = context
        self.effect = effect

    def add(self, t2, args):
        self.        context.effects.add_effect(self.effect, self.context.time, t2, args)

    def add_live(self, sbank, arg):
        from csound import CSOUND_LIVE_INFINITY
        self.context.update_effect_sliders(sbank, self.effect, 0)
        self.add(CSOUND_LIVE_INFINITY, arg)
        
    def slide(self, slider, val, dur):
        self.context.effects[self.effect][slider].add(FaderSegment(self.context.time, val, dur))
        self.context.mark_global_effects_dirty()
        

    def remove(self, t2):
        self.context.effects.remove_effects(self.effect, self.context.time, t2)
        self.context.mark_global_effects_dirty()


    def remove_slides(self, slider, t2):
        self.context.effects[self.effect][slider].remove_between(self.context.time, t2)
        self.context.mark_global_effects_dirty()

        
class CurrentEffectCommand(Command):

    name = 'current_effect'
    aliases = ('cfx',)
    arguments = StringArgument

    def execute(self, arg, context):
        context.current_effect = CurrentFaderEffect(arg, context)

command_list.add_command(CurrentEffectCommand)

class GlobalEffectCommand(Command):

    name = 'global_effect'
    aliases = ('gfx',)

    arguments = StringArgument
    
    def execute(self, arg, context):
        context.current_effect = CurrentGlobalEffect(arg, context)

command_list.add_command(GlobalEffectCommand)

class AddEffectCommand(Command):

    name = 'add_effect'
    aliases = ('afx',)
    arguments = ListArgument(NumberArgument, (StringArgument, ""))

    def execute(self, arg, context):
        t2, args = arg
        context.current_effect.add(t2, args)
        context.rescore = True

command_list.add_command(AddEffectCommand)

class RemoveEffectCommand(Command):

    name = 'remove_effect'
    aliases = ('rfx',)
    arguments = NumberArgument

    def execute(self, arg, context):
        context.current_effect.remove(arg)


command_list.add_command(RemoveEffectCommand)

def slider_commands(slider):
    class AddSliderCommand(Command):

        name = 'slider_{}'.format(slider)
        aliases = ('s{}'.format(slider),)
        arguments = ListArgument(NumberArgument, (NumberArgument, 0.0))

        def execute(self, arg, context):
            val, dur = arg
            context.current_effect.slide(slider, val, dur)


    command_list.add_command(AddSliderCommand)

    class RemoveSlidersCommand(Command):

        name = 'remove_sliders_{}'.format(slider)
        aliases = (
            'remove_sliders{}'.format(slider),
            'rs{}'.format(slider))
        arguments = NumberArgument

        def execute(self, arg, context):
            context.current_effect.remove_slides(slider, arg)

    command_list.add_command(RemoveSlidersCommand)

for s in range(1,effects.SLIDERS+1):
    slider_commands(s)
    
class EffectsOnCommand(Command):

    name = "effects_on"
    aliases = ("e+",)

    def execute(self, arg, context):
        context.current.effects_enabled  = True
        context.mark_current_dirty()

command_list.add_command(EffectsOnCommand)

class EffectsOffCommand(Command):

    name = "effects_off"
    aliases = ('e-',)

    def execute(self, arg, context):
        context.current.effects_enabled = False
        context.rescore = True

command_list.add_command(EffectsOffCommand)

@command_list.add_command
class LiveEffectCommand(Command):

    name = "live_effect"
    aliases = ('lfx',)
    arguments = ListArgument(StringArgument, StringArgument, StringArgument) #effect sliderbank, args

    def execute(self, arg, context):
        effect, sliders, args = arg
        cfe = CurrentFaderEffect(effect, context)
        cfe.add_live(sliders, args)
        context.current_effect = cfe
        context.mark_current_dirty()
        


@command_list.add_command
class EndLiveEffectCommand(Command):

    name = "end_live_effect"

    aliases = ('lfx-', 'elfx')

    arguments = StringArgument

    def execute(self, arg, context):
        context.current.end_live_effect(arg, context.time)
        context.mark_current_dirty(rescore = False)
        
@command_list.add_command
class GlobalLiveEffectCommand(Command):

    name = "global_live_effect"
    aliases = ('glfx',)
    arguments = ListArgument(StringArgument, StringArgument, StringArgument) #effect sliderbank, args

    def execute(self, arg, context):
        effect, sliders, args = arg
        gfe = CurrentGlobalEffect(effect, context)
        gfe.add_live(sliders, args)
        context.current_effect = gfe
        context.mark_global_effects_dirty()
        


@command_list.add_command
class EndGlobalLiveEffectCommand(Command):

    name = "end_global_live_effect"

    aliases = ('glfx-', 'eglfx')

    arguments = StringArgument

    def execute(self, arg, context):
        context.effects.end_live(arg, context.time)
        context.mark_global_effects_dirty()
        
