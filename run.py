import argparse, asyncio, os, os.path, sys, traceback, queue
from prompt_toolkit import prompt_async, history
import sh, time
from tempfile import NamedTemporaryFile
from ui.interface import *
from commands import fader_char
from config import config_obj

from context import context
import context_commands
import fader_commands
import cue_list_commands
import effects_commands
import solo_commands
import cue_point_commands
import preview_commands

context.config = config_obj

import csound
import cue_points


loop = asyncio.get_event_loop()
def print_exc(future):
    try: future.result()
    except Exception as e:
        traceback.print_exc()


def execute_command(text):
    print("{}> @{:.3f} {}".format(
        fader_char(context.fader),
        context.time, text))
    task = loop.create_task(command_list.execute(text, context))
    return task


    
async def run():
    try:
        context.now = time.time()
        while True:
            our_prompt = "{}> @{:.3f}".format(fader_char(context.fader), context.time)
            try:
                text = await prompt_async(our_prompt, complete_while_typing = True,
                                          history = history_file,
                                          completer = CommandListCompleter(),
                                          patch_stdout = (not args.debug))
                if context.now is None:
                    context.now = time.time()
                else:
                    now = time.time()
                    context.time += (now-context.now)
                    context.now = now
                task = execute_command(text)
                await task
            except EOFError:
                return
            except Exception as e:
                print(str(e))
                if args.debug: raise e

            if not context.rescore: continue
            csound_task = loop.create_task(csound.run_csound(context, execute_command, context.config))
            csound_task.add_done_callback(print_exc)
    finally:
        if csound.csound_cmd is not None:
            if csound.csound_cmd.process.is_alive()[0]: csound.terminate_csound()
        #asyncio.get_event_loop.create_task(task)

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
                    action = 'store_true',
                    help = 'Permit a debugger to be attached.')
parser.add_argument('--channels_4', '-4', action='store_true',
                    help = "4 channel real headphone mode")
parser.add_argument('--live',
                    help = 'live mode',
                    action = 'store_true')

parser.add_argument('--jack', action='store_true')
parser.add_argument('--config')


args = parser.parse_args()
context.debug = args.debug
if args.config:
    config_obj.load(args.config)

for k in ('live', 'jack', 'channels_4'):
    if getattr(args, k): #is true
        setattr(config_obj, k, True)
        

cue_points.setup()
history_file = history.FileHistory(os.path.expanduser("~/.clidj_history"))
asyncio.get_event_loop().run_until_complete(run())
