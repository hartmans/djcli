A command line DJ program.

Written because there doesn't appear to be any software usable by
someone who is blind that can easily mix tracks and adjust tempo for
beat matching.

Almost certainly only usable at this point by me.

Licensed under the GPLv3.

Copyright 2018 by Sam Hartman.

This software depends on a modified version of the python sh module
that allows a RunningCommand to be awaited (call command.wait in an
async compatible manner) and that supports async for for iterating
over command output.  That code is not checked into this repository.
If you use this software before I get around to cleaning that up and
submitting a pull request upstream, write your own version or fix up
the code not to need it.

