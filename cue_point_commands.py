
from ui.interface import *
from commands import QueueSongCommand

from cue_points import CuedSongCompleter

class StoreCuePointCommand(Command):

    name = 'store_cue_point'
    aliases = ('scp',)
    arguments = ListArgument(NumberArgument, OffsetArgument)


    def execute(self, args, context):
        context.current.update_cue_point(int(args[0]) if args [0]else None,
                                         float(args[1]) if args[1] else context.time)

command_list.add_command(StoreCuePointCommand)

class DeleteCuePointCommand(Command):

    name = 'delet_cue_point'
    aliases = ('dcp',)
    arguments = NumberArgument

    def execute(self, args, context):
        context.current.delete_cue_point(int(args))

command_list.add_command(DeleteCuePointCommand)

class AddCuePointsCommand(Command):

    name = 'add_cue_points'

    def execute(self, args, context):
        for t in context.all_tracks:
            if t.song.cp_header is None: continue
            if t.cue_point >= 0:
                t.update_cue_point(None, t.start)

command_list.add_command(AddCuePointsCommand)

class NextCuePointCommand(Command):

    name = 'next_cue_point'
    aliases = ('ncp',)

    def execute(self, args, context):
        for cp in context.current.song.cp_header.cue_points:
            if cp.t > context.current.jump_point:
                context.current.jump_point = cp.t
                print("Jump point: {}".format(cp.t))
                context.mark_current_dirty(restart_preview = True)
                if context.preview is not context.current:
                    context.play_excerpt(context.current, cp.t)
                return

command_list.add_command(NextCuePointCommand)

class PreviousCuePointCommand(Command):

    name = 'previous_cue_point'

    aliases = ('pcp',)

    def execute(self, arg, context):
        cps = [cp.t for cp in context.current.song.cp_header.cue_points]
        if (len(cps) == 0) or int(cps[0]) != 0:
            #Note that if a cue point is in the first second we don't force a 0
            cps.insert(0,0)
        for cp in reversed(cps):
            if cp < context.current.jump_point:
                context.current.jump_point = cp
                print("Jump point: {}".format(cp))
                context.mark_current_dirty(restart_preview = True)
                if context.preview is not context.current:
                    context.play_excerpt(context.current, cp)
                return

command_list.add_command(PreviousCuePointCommand)


class QueueCuedCommand(QueueSongCommand):

    name = 'queue_from_cue_points'
    aliases = ('qc',)
    arguments = CuedSongCompleter

command_list.add_command(QueueCuedCommand)

class TrackTempoCommand(Command):

    name = 'track_tempo'
    aliases = ('tempo',)

    arguments = NumberArgument

    def execute(self, args, context):
        context.current.song.track_tempo = args

command_list.add_command(TrackTempoCommand)
