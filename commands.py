import yaml
from ui.interface import *
from context import context
from songutils import PlayingSong, Song, match_songs
from prompt_toolkit.completion import Completion
from ui.time import Resolvable
from cue_points import CuedSongCompleter
import config


import csound

def fader_char(i):
    return chr(96+i)


def faderCommand(i):
    n = fader_char(i)
    class FaderCommand(Command):
        name = n
        def execute(self, args, context):
            if self.fader == context.other_fader_idx: context.other_fader_idx = context.fader
            context.fader = self.fader
            context.current = context.shortcuts[self.fader]
            context.mark_globals_dirty()
    command = FaderCommand()
    command.fader = i
    command_list.add_command(command)


class OtherFaderCommand(Command):
    name = 'other'
    arguments = StringArgument
    def execute(self,arg, context):
        fader = ord(arg)-ord('a')
        assert 0 < fader <= 10
        self.other_fader = fader

command_list.add_command(OtherFaderCommand)


class QueueSongCommand(Command):
    #No really, queue not cue
    name = 'queue'
    aliases = ('q',)
    arguments = FileArgument(['/home/hartmans/Music',
                              "/home/hartmans/beatport"])
    async def execute(self,args, context):
        song = await Song(args)
        old_track = context.current
        context.current = context.shortcuts[context.fader] = PlayingSong(song, context.fader)
        context.current.fades.initial = context.initial_fades[context.fader]
        context.current.output = 1
        if old_track is not None:
            context.mark_dirty(old_track)



command_list.add_command(QueueSongCommand)

for i in range(1,11):
    faderCommand(i)

class StartCommand(Command):
    name = 'start'
    

    def execute(self, args, context):
        context.current.start = context.time
        context.current.is_placed = True
        if context.current is context.preview:
            context.preview = None
        if context.current not in context.all_tracks:
            context.all_tracks.append(context.current)
        context.mark_current_dirty()
            

command_list.add_command(StartCommand())

class DiscardSongCommand(Command):
    name = "discard_song"
    aliases = ('discard',)
    def execute(self, arg, context):
        try:
            context.all_tracks.remove(context.current)
        except ValueError: pass
        context.mark_silent(context.current)
        context.current = None
        context.shortcuts[context.fader] = None

command_list.add_command(DiscardSongCommand)

class TimeCommand(Command):
    name = 'time'
    aliases = ('t',)
    arguments = TimeArgument

    def execute(self, args, context):
        if isinstance(args, Resolvable):
            args = args.resolve(context)
        context.time = args
        context.now = None
        context.rescore = True

command_list.add_command(TimeCommand)


class StopCommand(Command):
    name = 'stop'

    def execute(self, arg, context):
        context.current.length = context.current.offset_at(context.time)
        context.rescore = True

command_list.add_command(StopCommand)

class VolumeCommand(Command):
    name = 'volume'
    aliases = ('vol',)
    arguments = NumberArgument

    def execute(self, arg, context):
        context.current.volume = arg
        context.mark_current_dirty()

command_list.add_command(VolumeCommand)

class SyncCommand(Command):
    name ='sync'
    arguments = ListArgument(OtherTrackArgument, (NumberArgument, ""))

    def execute(self, arg, context):
        time = context.time
        current = context.current
        other, force_tempo = arg
        if force_tempo == 0: force_tempo = current.scale
        orig_scale = current.scale
        o1, o2, tempo_1, tempo_2, scale = match_songs(other.song, current.song,
                                                    other.offset_at(time),
                                                      current.offset_at(time),
                                                      other.scale, force_tempo)
        t1 = other.time_of(o1, after = context.time)
        try:
            current.scale = scale
            t2 = current.time_of(o2, after = context.time)
            # Sometimes with the scale adjustment, that beat will have already played
        except csound.time_utils.NeverHappens:
            o1, o2, tempo_1, tempo_2, scale = match_songs(other.song, current.song,
                                                          other.offset_at(time),
                                                          current.offset_at(time),
                                                          other.scale, force_tempo)
            t1 = other.time_of(o1, after = context.time)
            t2 = current.time_of(o2, after = time)
            
        finally: current.scale = orig_scale
        adjust =(t1-t2)/scale
        print("Sync {o2}  at {o1} adjusting by {adjust} ({tp1}/{tp2} = {scale})".format(
            tp1 = tempo_1,
            tp2 = tempo_2,
            scale = scale,
                o1 = o1,
                o2 = o2,
            adjust = adjust))
        if abs(adjust) >= 7:
            print('adjustment too large, not syncing')
            print('Currently at {o2} in current, {o1} in other.'.format(
                o2 = current.offset_at(time),
                o1 = other.offset_at(time)))
            
        current.scale = scale
        current.adjust_jump_by(adjust, time = context.time)

        context.mark_current_dirty()

command_list.add_command(SyncCommand)

class CuePointCommand(Command):
    name ='cue_point'
    aliases = ('cp'),
    
    arguments = OffsetArgument

    def execute(self, arg, context):
        if isinstance(arg, Resolvable):
            arg = arg.resolve(context)
        context.current.cue_point = arg
        context.mark_current_dirty(restart_preview = True)
        

command_list.add_command(CuePointCommand)


class SilenceCommand(Command):
    name = 'silence'
    aliases = ('sl', )

    def execute(self, arg, context):
        csound.terminate_csound()
        
command_list.add_command(SilenceCommand)

class InfoCommand(Command):
        

    name = 'info'
    def execute(self, arg, context):
        s = context.current
        def safe(f):
            try:
                return f()
            except: return "NA"
        print('''Song: {s}
Start at: {start}
Length to play: {length}
Last Time: {last}
Momentary Tempo: {tempo}
Track Tempo: {track_tempo}
Speed Factor: {speed}
Cue Point: {offset}
Offset @{time}: {offset_at}
        '''.format(
            s = s.song.filename,
            start = s.start,
            length = s.length,
            last = safe(lambda: s.last_time),
            time = context.time,
            offset = s.cue_point,
            speed = s.scale,
            tempo = safe(lambda: s.song.tempo_at(s.offset_at(context.time))),
            track_tempo = safe(lambda: s.song.track_tempo),
            offset_at = safe(lambda: s.offset_at(context.time))))
        try:
            cue_points = s.cue_point_offsets
            print('cue_points:')
            for idx, t in enumerate(cue_points):
                print("\t{}: {:.3f}".format(idx+1,t))
        except: pass
        if s.fades.list:
            last_fade = s.fades.list[-1]
            try:
                if last_fade.val == 0:
                    print("Fade out at t = {}".format(
                        s.time_of(last_fade.start + last_fade.dur)))
            except: pass
            

command_list.add_command(InfoCommand)


class KeysCommand(Command):

    name = 'keys'
    arguments  = NumberArgument

    def execute(self, arg, context):
        context.current.song.keys_from(arg)

command_list.add_command(KeysCommand)

class AllTracksArgument:

    def complete(self, document, event):
        text = document.text_before_cursor
        for i, t in enumerate(context.all_tracks):
            display = '{}: {}'.format(i+1, t.name)
            if text.lower() in display.lower():
                yield Completion(display, -len(text))

    def parse(self, text):
        num, sep, tail = text.partition(':')
        index = int(num)-1
        return context.all_tracks[index]
    
class SelectCommand(Command):

    name ='select'
    arguments = AllTracksArgument

    def execute(self, arg, context):
        for i, v in enumerate(context.shortcuts):
            if i == context.fader: continue
            if v == arg:
                print('Fader {} cleared because it points to the same track'.format(fader_char(i)))
                context.shortcuts[i] = None
        old_track = context.current
        context.current = context.shortcuts[context.fader] = arg
        context.mark_current_dirty(rescore = False)
        if old_track is not None:
            context.mark_dirty(old_track, rescore = False)
        
command_list.add_command(SelectCommand)

class AdjustByCommand(Command):
    name = "adjust_by"
    arguments = NumberArgument

    def execute(self, arg, context):
        for t in context.all_tracks:
            if t.start > context.time:
                t.start += arg

command_list.add_command(AdjustByCommand)

class OutputCommand(Command):

    name = 'output'
    arguments = StringArgument

    async def execute(self, arg, context):
        config_obj = config.Config()
        try:csound.terminate_csound()
        except: pass
        print('Starting csound out to {}'.format(arg))
        context.headphone_mix = 1.0
        context.current_headphone_volume = 0.0
        await csound.run_csound(context, None, output = '-o'+arg, config = config_obj)
        await csound.csound_cmd
        print('Output complete.')
        

command_list.add_command(OutputCommand)
class PrintCsoundCommand(Command):

    name = 'print_csound'

    arguments = StringArgument
    def execute(self, arg, context):
        if not arg:
            print(csound.csound_output)
        else:
            with open(arg, "w") as f:
                f.write(csound.csound_output)

command_list.add_command(PrintCsoundCommand)
class DacOutputCommand(Command):

    name = 'dac'
    arguments = StringArgument

    def execute(self, arg, context):
        if arg:
            csound.dac = '-odac:{}'.format(arg)
            print("Setting DAC")
        else: csound.dac =  '-odac'
        context.rescore = True

command_list.add_command(DacOutputCommand)

class MetronomeCommand(Command):

    name ='metro'
    arguments = NumberArgument

    def execute(self, arg, context):
        context.metronome = arg
        context.rescore = True

command_list.add_command(MetronomeCommand)

class DiscardFromHereCommand(Command):

    name = 'discard_from_here'

    aliases = ('del>', 'dfh')

    def execute(self, arg, context):
        new_tracks = []
        for t in context.all_tracks:
            if t.start > context.time:
                context.mark_silent(t)
            else: new_tracks.append(t)
        context.all_tracks = new_tracks
        for i, v in enumerate(context.shortcuts):
            if v not in context.all_tracks:
                context.shortcuts[i] = None
                

command_list.add_command(DiscardFromHereCommand)

        

class PitchCommand(Command):

    name = 'pitch_factor'
    aliases = ('pitch', 'scale')

    arguments = NumberArgument
    def execute(self, arg, context):
        context.current.scale = arg
        context.mark_current_dirty()

command_list.add_command(PitchCommand)
    
class JumpCommand(Command):

    name = 'jump'

    aliases = ('jmp',)

    def execute(self, arg, context):
        if context.current.is_placed:
            context.current.add_jump(context.time, crossfade=context.crossfade_time)
        else: context.place(context.current, context.time, context.current.jump_point)
        context.mark_current_dirty()

command_list.add_command(JumpCommand)

class RemoveJumpCommand(Command):

    name = 'remove_jumps'

    aliases = ('rj', 'rjp', 'remove_jump')

    arguments = TimeArgument

    def execute(self, arg, context):
        if arg is None: arg = context.time
        context.current.remove_jumps(context.time, arg)
        context.mark_current_dirty(rescore = False)

command_list.add_command(RemoveJumpCommand)

class JumpPointCommand(Command):

    name = 'jump_point'

    aliases = ('jp',)
    arguments = OffsetArgument

    def execute(self, arg, context):
        if isinstance(arg, Resolvable):
            arg = arg.resolve(context)
        context.current.jump_point = arg

command_list.add_command(JumpPointCommand)

class LoopCommand(Command):

    name = 'loop'
    arguments = OffsetArgument

    def execute(self, arg, context):
        if arg is None: arg = context.current.offset_at(context.time)
        context.current.set_loop_point(arg)
        context.mark_current_dirty()

command_list.add_command(LoopCommand)

class NoLoopCommand(Command):

    name = 'no_loop'

    aliases = ('loop-', 'l-')

    def execute(self, arg, context):
        context.current.loop_point = None
        context.mark_current_dirty(rescore = False)
        
command_list.add_command(NoLoopCommand)
class TransposeCommand(Command):

    name = 'transpose'
    arguments = NumberArgument

    def execute(self, arg, context):
        context.current.transpose = arg
        context.mark_current_dirty()

command_list.add_command(TransposeCommand)

class SplitCommand(Command):

    name = 'split'
    arguments = TimeArgument

    async def execute(self, t, context):
        current = context.current
        assert current.is_placed
        new = yaml.load(yaml.dump(current))
        await new.song_waiter
        if t is None: t = context.time
        
        cue_point = current.track_time_at(t)
        current.length = cue_point
        new.start = t
        new.cue_point = cue_point
        new.break_at = False
        context.all_tracks.append(new)

        context.mark_silent(current)
        context.current = context.shortcuts[context.fader] = new
        context.mark_current_dirty()

command_list.add_command(SplitCommand)


class AddToListCommand(Command):

    name ='add_to_list'
    aliases = ('a2l', 'add')
    arguments = CuedSongCompleter


    async def execute(self, arg, context):
        if arg not in context.play_list:
            await Song(arg)
            context.play_list.append(arg)
            context.play_list_index = len(context.play_list)-1

command_list.add_command(AddToListCommand)

class ListNextCommand(QueueSongCommand):

    name = 'list_next'
    aliases =('list+',)

    async def execute(self, arg, context):
        if context.current and context.current.is_placed and context.time <context.current.last_time:
            raise RuntimeError("Track Not Yet Ended")
        context.play_list_index += 1
        context.play_list_index = context.play_list_index % len(context.play_list)
        arg = context.play_list[context.play_list_index]
        await super().execute(arg, context)
        context.play_excerpt(context.current, context.current.excerpt_point)
        print(context.current)

command_list.add_command(ListNextCommand)

class ListPrevCommand(QueueSongCommand):

    name = 'list_prev'
    aliases =('list-',)

    async def execute(self, arg, context):
        if context.current and context.current.is_placed and context.time <context.current.last_time:
            raise RuntimeError("Track Not Yet Ended")
        context.play_list_index -= 1
        context.play_list_index = context.play_list_index % len(context.play_list)
        arg = context.play_list[context.play_list_index]
        await super().execute(arg, context)
        context.play_excerpt(context.current, context.current.excerpt_point)
        print(context.current)

command_list.add_command(ListPrevCommand)


class CrossFadeCommand(Command):

    arguments = NumberArgument
    name = 'crossfade'

    def execute(self, arg, context):
        if arg is None: arg = 0
        context.crossfade_time = arg

command_list.add_command(CrossFadeCommand)

@command_list.add_command

class GlobalLoopCommand(Command):

    name = 'global_loop'
    aliases = ('gloop',)

    arguments = ListArgument(NumberArgument, NumberArgument)

    def execute(self, arg, context):
        context.global_loop = (arg[0], arg[1])

@command_list.add_command
class NoGlobalLoopCommand(Command):

    name = 'no_global_loop'
    aliases = ('gloop-',)

    def execute(self, arg, context):
        context.global_loop = None
        
