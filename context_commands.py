from ui.interface import *
import yaml

from context import context


class LoadCommand(Command):
    name = 'load'
    arguments = FileArgument('.')

    def execute(self, args, context):
        context.load(args)

command_list.add_command(LoadCommand)

class LoadKeepTimeCommand(Command):

    name = 'load_keep_time'
    aliases = ('lkt',)
    arguments = FileArgument(".")


    async def execute(self, arg, context):
        old_tracks = context.all_tracks
        if context.now is None:
            raise ValueError("Wait for time to start")
        t = context.time
        context.load(arg)
        context.time = t
        for t in context.all_tracks:
            if t.song_waiter is not None: await t.song_waiter
        for t in old_tracks:
            t.mute = True
            context.mark_dirty(t)
        for t in context.all_tracks:
            t.output = 0
            context.mark_dirty(t)
            
command_list.add_command(LoadKeepTimeCommand)

class LoadFindCurrentCommand(Command):

    name = 'load_find_current'
    aliases = ('lfc',)
    arguments = FileArgument(".")


    async def execute(self, arg, context):
        old_tracks = context.all_tracks
        current = context.current.song
        if context.now is None:
            raise ValueError("Wait for time to start")
        from context import Context
        new_context = Context()
        new_context.load(arg)

        for t in new_context.all_tracks:
            if t.song_waiter is not None: await t.song_waiter
        sync_time = None
        offset = context.current.offset_at(context.time)
        track_time = context.current.track_time_at(context.time)
        for t in new_context.all_tracks:
            if t.song is current:
                try:
                    sync_time = t.time_of(offset)
                    cue_point = t.track_time_at(sync_time)
                    if sync_time > t.last_time: continue
                    adjust = context.time-sync_time
                except: continue
                context.current.length = track_time
                t.start = sync_time
                t.cue_point = cue_point
                t.volume = context.current.volume
                t.break_at = False
                context.mark_silent(context.current)
                context.current = t
                break
            
        if sync_time is None:
            print('Unable to sync to new project')
            return
        for t in new_context.all_tracks:
            if t.start >= sync_time:
                t.start += adjust
                context.all_tracks.append(t)
                context.mark_dirty(t)

command_list.add_command(LoadFindCurrentCommand)





class SaveCommand(Command):

    name ='save'
    arguments = StringArgument
    
    def execute(self, args, context):
        with open(args, 'wt') as f:
            d = dict(context.__dict__)
            del d['config']
            f.write(yaml.dump(d, default_flow_style = False))

command_list.add_command(SaveCommand)

class ImportCommand(Command):

    name = 'import'

    arguments = FileArgument(".")


    async def execute(self, arg, context):
        from context import Context
        new_context = Context()
        new_context.load(arg)

        for t in new_context.all_tracks:
            if t.song_waiter is not None: await t.song_waiter
        adjust = context.time
        min_start = -1
        min_start_track = None
        for t in new_context.all_tracks:
            t.start += adjust
            if (min_start == -1) or t.start < min_start:
                min_start = t.start
                min_start_track = t
            context.all_tracks.append(t)
            context.mark_dirty(t)

        old_current = context.current
        context.current = context.shortcuts[context.fader] = min_start_track
        context.mark_current_dirty()
        if old_current: context.mark_dirty(old_current)

command_list.add_command(ImportCommand)
