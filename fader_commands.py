from ui.interface import *
from fader import FaderSegment

class FadeToCommand(Command):

    name = "fade_to"
    aliases = ('f2',)
    arguments = ListArgument(NumberArgument, (NumberArgument, 0.5))
    
    def execute(self, args, context):
        val, dur = args
        context.current.add_fade(context.time, val, dur)
        context.mark_current_dirty()
        

command_list.add_command(FadeToCommand)

class FadersOnCommand(Command):
    name = 'faders_on'
    aliases = ('faders', 'f+')

    def execute(self, arg, context):
        context.enable_faders = True
        context.rescore = True
command_list.add_command(FadersOnCommand)

class FadersOffCommand(Command):
    name = 'faders_off'
    aliases = ('nofaders', 'f-')

    def execute(self, arg, context):
        context.enable_faders = False
        context.rescore = True
command_list.add_command(FadersOffCommand)


class RemoveFadesCommand(Command):
    name = 'remove_fades'
    aliases = ('rfd',)
    arguments = NumberArgument

    def execute(self, arg, context):
        context.current.remove_fades(context.time, arg)
        context.mark_current_dirty()

command_list.add_command(RemoveFadesCommand)

class InitialCommand(Command):
    name = 'initial_fade'
    aliases = ('initial',)
    arguments = NumberArgument

    def execute(self, arg, context):
        context.current.fades.initial = float(arg)
        context.initial_fades[context.fader] = float(arg)

command_list.add_command(InitialCommand)
