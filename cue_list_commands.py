from cue_list import *
from ui.interface import *

class CueListCommand(Command):

    name = 'cue_list'
    arguments = StringArgument

    def execute(self, arg, context):
        cue_list = CueList(context.all_tracks)
        cue_list.print_markdown(arg)

command_list.add_command(CueListCommand)

class SplitTracksCommand(Command):

    name = 'split_tracks'

    async def execute(self, arg, context):
        cue_list = CueList(context.all_tracks)
        await cue_list.split(context)

command_list.add_command(SplitTracksCommand)

class NoBreakCommand(Command):

    name = 'no_break'
    aliases = ('nbrk', 'break-')

    def execute(self, arg, context):
        context.current.set_break_time(False)

command_list.add_command(NoBreakCommand)

class BreakLastCommand(Command):

    name = 'break_last'

    def execute(self, arg, context):
        context.current.set_break_time(True)

command_list.add_command(BreakLastCommand)

class BreakHereCommand(Command):

    name = 'break_here'

    aliases = ('break',)

    def execute(self, arg, context):
        context.current.set_break_time(context.time)

command_list.add_command(BreakHereCommand)

class AlbumCommand(Command):

    name = 'album'
    arguments = StringArgument

    def execute(self, arg, context):
        context.album = arg

command_list.add_command(AlbumCommand)

class RemixedByCommand(Command):

    name = 'remixed_by'
    arguments = StringArgument

    def execute(self, arg, context):
        context.remixed_by = arg

command_list.add_command(RemixedByCommand)

class CueSheetCommand(Command):

    arguments = StringArgument

    name = 'cue_sheet'

    def execute(self, arg, context):
        cue_list = CueList(context.all_tracks)
        cue_list.print_cue_sheet(arg)

command_list.add_command(CueSheetCommand)

