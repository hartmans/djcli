import yaml

class Config:

    effects = []
    jack = False
    live = False
    channels_4 = False
    midi_device = None
    dac = ""
    adc = None

    def load(self, f):
        with open(f) as fin:
            d = yaml.load(fin)
        for k in d:
            setattr(self, k, d[k])

config_obj = Config()
