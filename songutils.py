import asyncio, re, tempfile, traceback, yaml
import  os.path
import sh
from yaml import YAMLObject
from fader import FaderList, FaderSegment
from effects import EffectList
import cue_points
from ui.time import CuePointMarker
from csound.time_utils import inner_time_at, outer_time_of, NeverHappens


_songs = {}
_song_dir = os.path.expanduser("~/.cache/djcli")

loop = asyncio.get_event_loop()

song_queue  = asyncio.Queue(loop = loop)

async def queue_worker():
    while True:
        item = await song_queue.get()
        file, future = item
        try:
            print('Loading {}'.format(file))
            s = await Song(file)
            future.set_result(s)
            print('Loaded {}'.format(file))
        except Exception as e:
            print('{} loading {}'.format(e, file))
            future.set_exception(e)

def queue_song(f):
    future = loop.create_future()
    song_queue.put_nowait((f, future))
    return future

for i in range(3):
    loop.create_task(queue_worker())

class Song:

    cache_keys = (
        'beats',
        'tempos',
        'keys',
        'length',
        'metadata')



    async def __new__(type, file):
        if file in _songs: return _songs[file]
        os.makedirs(_song_dir, exist_ok = True)
        self = super().__new__(type)
        self.filename = file
        base = os.path.basename(file)
        base = base.replace(' ','')
        base = base[0:base.rfind('.')]
        self.wav = os.path.join(_song_dir, base+".wav")
        if not os.path.exists(self.wav):
            await sh.sox(file,
                         '-e', 'float', '-b32', '-r48000',
                         self.wav,
                         "--norm", "dither",
               _bg = True,
               )
        cache_file = os.path.join(_song_dir, base+'.cache')
        cache_loaded = False
        if os.path.exists(cache_file):
            try:
                with open(cache_file, "rt") as f:
                    obj = yaml.load(f.read())
                    for k in self.cache_keys:
                        setattr(self, k,obj.get(k))
                    cache_loaded = True
            except Exception:
                print('Ignoring cache for {}'.format(base))
                traceback.print_exc()
        if not cache_loaded:
            self.metadata = {}
            info = self._compute_info()
            self.tempos = []
            self.beats = []
            self.keys = []
            beats =  self._process_beats(sh.vamp_simple_host("libmixxxminimal:qm-tempotracker", self.wav,
                            "0", _iter = True))
            tempo =         self._process_tempos(sh.vamp_simple_host("libmixxxminimal:qm-tempotracker", self.wav,
                                                "2", _iter = True))
            keys =         self._process_keys(sh.vamp_simple_host("libmixxxminimal:qm-keydetector", self.wav,
                                                "2", _iter = True))
            await asyncio.gather(tempo, beats, info, keys)
            with open(cache_file, "wt") as f:
                f.write(yaml.dump(
                    {k: getattr(self,k) for k in self.cache_keys},
                    default_flow_style = False))

        # Load cue points
        session = cue_points.sessions()
        try:
            self.cp_header = cue_points.CuePointHeader.get_or_create({
                'title': self.metadata['title'],
                'artist': self.metadata.get('artist', None),
                'album': self.metadata.get('album', None)}, session)
            session.commit()
            session.refresh(self.cp_header)
            session.close()
            if self.cp_header.file_name != file:
                self.cp_header.update(file_name = file)
        except KeyError:
            self.cp_header = None
        del session

        _songs[file] = self
        return self



    async def _compute_info(self):
        duration_cmd = sh.soxi("-D", self.filename, _bg = True)
        metadata_cmd = sh.soxi('-a', self.filename, _iter = True)
        async for l in metadata_cmd:
            l = l.strip()
            if '=' not in l: continue
            k,v = re.split('=\s*', l, 1)
            k = k.lower()
            self.metadata[k] = v
        await duration_cmd
        self.length = float(duration_cmd.stdout)

    async def _process_beats(self, command):
        async for l in command:
            f = l.split(None,2)
            if len(f) < 2: continue
            f[0] = float(f[0][:-1])
            f[1] = float(f[1])
            self.beats.append(f[0])

    async def _process_keys(self, command):
        async for l in command:
            l = l.strip()
            f = l.split(None,2)
            if len(f) < 3: continue
            f[0] = float(f[0][:-1])
            f[1:] = [f[2]]
            self.keys.append(f)



    async def _process_tempos(self, command):
        async for l in command:
            f = l.split(None,2)
            f[0] = float(f[0][:-1])
            f[1] = float(f[1])
            self.tempos.append((f[0], f[1]))



    def tempo_at(self, time):
        tempo = 0
        for t_time, t in self.tempos:
            if t_time > time: break
            tempo = t
        return tempo

    def keys_from(self, time, num_keys = 7):
        key = None
        initial = True
        for t, k in self.keys:
            if  t >= time:
                if initial:
                    initial = False
                    print("Prior key: {}".format(key))


                print("{:.3f}\t{}".format(t, k))
                num_keys -= 1
                if num_keys <= 0: break
            key = k

    @property
    def track_tempo(self):
        "A tempo that overrides per-item tempos"
        if self.cp_header is None: return None
        return self.cp_header.bpm

    @track_tempo.setter
    def track_tempo(self, bpm):
        self.cp_header.update(bpm = bpm)

    @property
    def name(self):
        if 'title' in self.metadata:
            return self.metadata['title']
        else:  return os.path.basename(self.filename)


def match_songs(s1, s2, offset_s1, offset_s2, scale_s1, force_tempo = None):
    for t1 in s1.beats:
        if t1 <= offset_s1: continue
        break
    tempo_1 = s1.tempo_at(t1)
    if s1.track_tempo is not None: tempo_1 = s1.track_tempo
    for t2 in s2.beats:
        if t2 <= offset_s2: continue
        break
    tempo_2 = s2.tempo_at(t2)
    if s2.track_tempo is not None: tempo_2 = s2.track_tempo
    difference_1 = t1-offset_s1
    difference_2 = t2-offset_s2
    scale = (tempo_1/tempo_2)*scale_s1
    if force_tempo is not None:
        if force_tempo > 2.0: scale = (tempo_1/force_tempo)*scale_s1
        else: scale = force_tempo
    return (t1, t2, tempo_1, tempo_2, scale)

class PlayingSong (YAMLObject):
    yaml_tag = 'playing_song'

    def __getstate__(self):
        return {
            'song': self.song.filename,
            'scale': self.scale,
            'cue_point': self.cue_point,
            'jump_point': self.jump_point,
            "jumps": self.jumps,
            'start': self.start,
            "is_placed": self.is_placed,
            'length': self.length,
            'break_at': self.break_at,
            'fader': self.fader,
            'volume': self.volume,
            'fades': self.fades,
            'effects': self.effects,
            'name': self._name,
            'transpose': self.transpose,
            }

    def __setstate__(self, state):
        def song_callback(fut):
            self.song = fut.result()
            self.song_waiter.set_result(True)
            self.song_waiter = None
        song = state.pop('song')
        song_waiter  = asyncio.get_event_loop().create_future()
        song = queue_song(song)
        song.add_done_callback(song_callback)
        self.song_waiter = song_waiter
        self.scale = 1.0
        self._jump_point  =0
        self._cue_point = 0
        self.is_placed = True
        self.loop_point = None
        for k,v in state.items():
            if k == 'offset': k = 'cue_point'
            setattr(self, k, v)
        self.mute = False
        if not hasattr(self, '_name'):
            self._name = None
        if not hasattr(self, 'fades'):
            self.fades = FaderList(self.fader)
        if not hasattr(self, 'transpose'):
            self.transpose = 1.0
        if not hasattr(self, 'effects'):
            self.effects = EffectList(self.fader)
        self.effects_enabled = True
        if not hasattr(self, 'break_at'): self.break_at = True
        if not hasattr(self, "_jump_point"): self.jump_point = self.cue_point
        if not hasattr(self, 'jumps'):
            self.jumps = FaderList(0, initial = 0)
            # But Also convert fades and effects to no longer be pitch scaled
            self.fades = self.fades.pitch_scale(1/self.pitch)
            self.effects.pitch_scale(1/self.scale)
        self.output = 0


    def __init__(self, song, fader, start = None, cue_point = 0, scale = 1.0, length = None, volume = 1.0):
        assert isinstance(song, Song)
        self.song = song
        self.volume = volume
        self.loop_point = None
        self.is_placed = start is not None
        self.jumps = FaderList(0, initial = 0)
        self.scale = scale
        self.cue_point = cue_point
        self.jump_point = cue_point
        self.start = start
        self.transpose = 1.0
        self._length = length
        self.fader = fader
        self._name = None
        self.fades = FaderList(self.fader)
        self.effects = EffectList(self.fader)
        self.effects_enabled = True
        self.mute = False
        self.break_at = True
        self.output = 0

    def __format__(self, fmt):
        n = self.name
        if self.song.track_tempo:
            n += "({} BPM)".format(self.song.track_tempo)
        return n

    def __str__(self):
        return format(self, "")


    @property
    def name(self):
        if self._name is not None:
            return self._name
        return self.song.name

    @name.setter
    def name(self, val):
        self._name = val

    def offset_at(self, time):
        "Return the file position (offset) at a given outer time"
        if isinstance(time, CuePointMarker):
            return self.cue_point_offset(time.cue_point)
        track_time = self.track_time_at(time)
        return inner_time_at(track_time, 0, 0, self.scale, discont = self.jumps)

    def track_time_at(self, t):
        "Track time is the time within  the track; the start time maps to the cue_point within track_time.  Track time progresses at one second per second; pitch scaling is applied to the file offset, not to track time."
        if isinstance(t, CuePointMarker):
            offset = self.cue_point_offset(t.cue_point)
            return outer_time_of(offset, 0, 0, self.pitch, discont = self.jumps)
        return inner_time_at(t, self.start, self.cue_point,1)


    def time_of(self, offset, *, after = None, track_after = None):
        assert after is None or track_after is None
        if after is not None:
            track_after = self.track_time_at(after)
        if track_after is None: #neither specified
            track_after = 0
        track_time = outer_time_of(offset, 0, 0, self.pitch, discont = self.jumps,
                                   after = track_after)
        return outer_time_of(track_time, self.start, self.cue_point, 1)

    def time_of_track(self, t, after = 0):
        "Return the time of a given track time"
        return outer_time_of(t, self.start, self.cue_point, 1, after = after)


    @property
    def length(self):
        if self._length is not None: return self._length
        return self.song.length

    @length.setter
    def length(self, new_len = None):
        self._length =new_len
        return self.length

    @property
    def last_jump_track_time(self):
        if len(self.jumps) == 0: return 0
        last_jump = self.jumps.list[-1]
        return last_jump.start

    def jump_before_track_time(self, t):
        track_time = self.track_time_at(t)
        seg = self.jumps.segment_before(track_time)
        if seg is None: return 0
        return seg.start

    def add_fade(self, time, val, dur):
        offset = self.track_time_at(time)
        self.fades.add(FaderSegment(offset, val, dur))


    def remove_fades(self, t1, t2):
        offset1 = self.track_time_at(t1)
        offset2 = self.track_time_at(t2)
        self.fades.remove_between(offset1, offset2)

    def add_jump(self, t, ft = None, *, crossfade = 0):
        "Add a jump from time t to file position ft"
        if ft is None: ft = self.jump_point
        if isinstance(ft, CuePointMarker):
            ft = self.cue_point_offset(ft.cue_point)
        seg = FaderSegment(self.track_time_at(t), ft, crossfade)
        self.jumps.add(seg)

    def remove_jumps(self, t1, t2):
        self.jumps.remove_between(
            self.track_time_at(t1),
            self.track_time_at(t2))


    def calc_last_time(self, ignore_fadeout = False):
# collect lengths from various sources and find the minimum
# First: song_max
        lengths = [self.time_of(self.song.length, track_after = self.last_jump_track_time)]
        if (not ignore_fadeout) and self.fades.list:
            last_fade = self.fades.list[-1]
            try:
                if last_fade.val == 0:
                    lengths.append(self.time_of_track(last_fade.start + last_fade.dur))
            except:
                pass
        if self._length:
            try:
                lengths.append(self.time_of(self._length, track_after = self.last_jump_track_time))
            except: pass
        return min(lengths)

    @property
    def last_time(self): return self.calc_last_time()

    def add_effect(self, effect, t1, t2, args):
        from csound import CSOUND_LIVE_INFINITY
        t1 = self.track_time_at(t1)
        if t2 == "infinity":
            t2 = CSOUND_LIVE_INFINITY
            if not self.is_placed: t1 = 0
        else: t2 = self.track_time_at(t2)
        self.effects.add_effect(effect,
                                t1,
                                    t2,
                                args)

    def remove_effect(self, effect, t1, t2):
        self.effects.remove_effects(effect,
                                    self.track_time_at(t1),
                                    self.track_time_at(t2))

    def end_live_effect(self, effect, t):
        track_time = self.track_time_at(t)
        self.effects.end_live(effect, track_time)

    def add_slide(self, effect, slider, time, val, dur):
        if time == "initial":
            self.effects[effect][slider].initial = val
        else:
            offset = self.track_time_at(time)
            self.effects[effect][slider].add(FaderSegment(offset, val, dur))

    def remove_slides(self, effect, slider, t1, t2):
        offset1 = self.track_time_at(t1)
        offset2 = self.track_time_at(t2)
        self.effects[effect][slider].remove_between(offset1, offset2)

    def filename_base(self, track):
        name = self.name
        name = re.sub( r' \?\/\.\:', '_', name)
        return "{:02}-{}".format(track, name)


    def break_time(self, last_ending):
        "Return time at which a new file should be started for this track.  Takes an input of the time at which the previous track faded out or ends"
        if self.break_at is False: return None
        if self.break_at is True:
            if self.start > last_ending: return self.start
            return last_ending
        #Otherwise break is an offset
        try:
            return self.time_of(self.break_at)
        except:
            self.break_at = True
            return last_ending

    def set_break_time(self, t):
        if t is True or t is False:
            self.break_at = t
        else: self.break_at = self.track_time_at(t)

    def set_jump_point(self, pos):
        if isinstance(pos,CuePointMarker):
            pos = self.cue_point_offset(pos.cue_point)
        self._jump_point = pos
        if not self.is_placed:
            try: self.cue_point = outer_time_of(pos, 0, 0, self.pitch,
                                                discont = self.jumps)
            except (AttributeError, NeverHappens): pass


    @property
    def jump_point(self):
        return self._jump_point

    @jump_point.setter
    def jump_point(self, pos):
        self.set_jump_point(pos)

    def set_loop_point(self, pos):
        if isinstance(pos,CuePointMarker):
            pos = self.cue_point_offset(pos.cue_point)
        self.loop_point = pos

    def update_cue_point(self, number, t):
        return self.song.cp_header.update_cue_point(number, t)

    def delete_cue_point(self, number):
        return self.song.cp_header.delete_cue_point(number)

    def cue_point_offset(self, number):
        cp = self.song.cp_header.cue_point(number)
        return cp.t

    def cue_point_time(self, number):
        return self.time_of(self.cue_point_offset(number))

    @property
    def cue_point_offsets(self):
        for cp in self.song.cp_header.cue_points:
            yield cp.t


    def adjust_jump_by(self, offset, time = None):
        if len(self.jumps) == 0:
            self.cue_point += offset
        else:
            if time is None:
                last_jump = self.jumps.list[-1]
            else: last_jump = self.jumps.segment_before(self.track_time_at(time))
            self.add_jump(self.time_of_track(last_jump.start),
                          last_jump.val+offset, crossfade=last_jump.dur)


    @property
    def pitch(self):
        "New name but we're still migrating from scale to pitch"
        return self.scale

    @property
    def excerpt_point(self):
        cps = list(self.cue_point_offsets)
        if cps: return cps[0]
        return 0





__all__ = ['Song', 'PlayingSong', 'match_songs']
