


from tempfile import NamedTemporaryFile

from .utils import EffectInstruments

from effects import SLIDERS, Effect
# These constants must be adjusted against the orchestra iteslf The
# local effects need to be between instruments generating audio and
# bus routing instruments; the global effects between bus routing and
# the master mix.
MaxEffects = 30
BusEffectBase = 20
MasterEffectBase = 60

# When adjusting the above consider the following instruments:
# 10: read sound file route to bus
# 58: route audio to master; apply fades
# 11: Instantiate bus effects
# 59: instantiate master effects
# 57: Effect sliders from tables

class CsoundOrchestra:

    def __init__(self, outputs, context):
        self.bus_instruments = EffectInstruments(BusEffectBase, MaxEffects)
        self.master_instruments = EffectInstruments(MasterEffectBase, MaxEffects)
        self.outputs = outputs
        if len(context.all_tracks) > self.outputs:
            self.outputs = len(context.all_tracks)  * 2
        self.outputs += 1 #for headphones
        self.f = NamedTemporaryFile("wt", encoding = 'utf-8')
        self.name = self.f.name

        effects = set(context.all_effects())
        for e in context.config.effects:
            effects.add(Effect(e))
        self.zks = self.number_effects(effects)
        self.effects = effects

    def number_effects(self, effects):
        base = self.outputs+1
        for e in effects:
            e.slider_base = base
            self.master_instruments.append(e)
            self.bus_instruments.append(e)
            base += SLIDERS*(self.outputs+1)
        return base

            
        

        
    def  gen_orc_file(self, base_time):
        f = self.f
        f.write("#define ZKS #{}#\n".format(self.zks))
        f.write("#define SLIDERS #{}#\n".format(SLIDERS))
        f.write("#define OUTPUTS #{outputs}#\n".format(outputs = self.outputs))
        f.write("#define DELTATIME #{}#\n".format(base_time))
        with open('mix.orc.in', 'rt', encoding = 'utf-8') as o_in:
            self.f.write(o_in.read())
        for e in self.effects:
            self.output_effect_instruments(e)
        self.f.flush()

    def output_effect_instruments(self, e):
        
        s = ""
        for inum in (self.bus_instruments[e], self.master_instruments[e]):
            s += '''
instr {inum}
        ihold
        isliderbase sliderbase {base}, p4
        is1 zir isliderbase
        is2 zir isliderbase+1
        is3 zir isliderbase+2
ks1 zkr isliderbase
        ks2 zkr isliderbase+1
        ks3 zkr isliderbase+2
        ileft init p4
        iright init gioutputs+p4
        a1 zar ileft
        a2 zar iright
        {code}
        zaw a1, ileft
        zaw a2, iright
        endin
'''.format(
            base = e.slider_base,
            code = e.code,
            inum = inum
            )
        self.f.write(s)

    def close(self):
        if self.f is not None:
            self.f.close()
            self.f = None
            
