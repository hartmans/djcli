import sh, re

info_re = re.compile(r'buffer size = (\d+)\s+sample rate =\s+(\d+)')

def jack_info():
    res = sh.jack_bufsize(_tty_out = False, _encoding ='utf8')
    match = info_re.search(str(res))
    return int(match.group(1)), int(match.group(2))

def kr(sr):
    if sr == 48000:
        return 250
    elif sr == 44100:
        return 100
