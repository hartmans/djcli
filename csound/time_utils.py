
from fader import FaderSegment

'''Conceptually this module deals with mapping an outer timeline to an inner timeline.  The inner timeline is glued at the start point (measured on the outer timeline) to a cue point (measured along the inner timeline) and scaled by a pitch scaling factor.  There may be a table of discontinuities where the inner time jumps at a given moment in outer time.  As a result, an inner time may correspond to 0, one or more outer times.  The cue_point could be viewed as such a discontinuity.

This is used as follows.  There is a global timeline which is the outer-most timeline.  Tracks have a track timeline.  The track timeline is mapped to file position timelines; it is this inner most mapping that may have discontinuities.

To avoid needing to copy FaderLists an inner timeline must not have a non-zero cue_point and a table of discontinuities.
'''



def min_inner_time(t, start, cue_point, pitch, *, discont = None):
    "return the mininum offset in a event set that will be rendered if we render starting at t, when the event set starts at start with a cue_point in the event's offset stream of cue_point"
    if t < start: return cue_point
    return inner_time_at(t, start, cue_point, pitch, discont = discont)

def inner_time_at(t, start, cue_point, pitch, *, discont = None):
    "Return a possibly negative offset assuming  outer time t a stream that starts in the outer time at start with an internal time of cue_point and is pitch-scaled by pitch."
    after_start = t-start
    if (discont is None) or (t < start):
        return cue_point + after_start*pitch
    discont2 = discont.copy()
    discont2.add(FaderSegment(start, cue_point, 0))
    return discont2.value_sloped(t, pitch)

class NeverHappens(Exception):

    def __init__(self):
        super().__init__("Offset before start")

def outer_time_of(offset, start, cue_point, pitch, *, discont = None, after = 0):
    outer_t = start
    inner_t = cue_point
    if start > after: after = start
    if discont is not None:
        for s, v, d in discont.after_time(0):
            delta = (offset-inner_t)/pitch
            if (delta >= 0) and (outer_t+delta <s) and (outer_t + delta >= after) :
                #We actually get as far as offset on the inner timeline
                break
            else:
                outer_t = s
                inner_t = v
    if inner_t > offset:
        raise NeverHappens()
    offset = offset-inner_t
    if outer_t+(offset/pitch) < after: raise NeverHappens()
    return outer_t+(offset/pitch)


class TimeAdapter:

    "View a stream of events with a local start time, durationn viewed through the lense of a particular cue_point in that stream mapped into an outer time with a given start.  An optional table of discontinuities is also supported."

    def __init__(self, inner, start = None, cue_point = None, pitch = None, discont = None):
        if start is None: start = inner.start
        if cue_point is None: cue_point = inner.cue_point
        if pitch is None: pitch = inner.pitch
        if discont is None: discont = getattr(inner, 'discont', None)
        self.inner = inner
        self.start = start
        self.cue_point = cue_point
        self.pitch = pitch
        self.discont = discont

    def inner_time_at(self, t):
        return inner_time_at(t, self.start, self.cue_point, self.pitch, discont = self.discont)

    def time_of(self, t):
        return outer_time_of(t, self.start, self.cue_point, self.pitch,
                             discont = self.discont)
    
    
    def after_time(self, time):
        "An iterator for all events that happen after time t or are happening at t"
        offset = min_inner_time(time, self.start, self.cue_point, self.pitch, discont = self.discont)
        for t, val, dur in self.inner.after_time(offset):
            assert t+dur >= offset
            try:
                outer_t = outer_time_of(t, self.start, self.cue_point, self.pitch, discont = self.discont)
            except NeverHappens:
                outer_t = self.start
                end = outer_time_of(t+dur, self.start, self.cue_point, self.pitch, discont = self.discont)
                outer_dur = end-self.start
            else:
                outer_dur = dur/self.pitch
            yield outer_t, val, outer_dur

    def __iter__(self):
        return self.after_time(0)


    def value_at(self, t):
        "Return the value at time t; returns the value at the cue point for all times prior to the cue point.  This is to be consistent with time_after which will not yield things that happen before the cue_point."
        if hasattr(self.inner, 'value_at'):
            return self.inner.value_at(min_inner_time(t, self.start, self.cue_point, self.pitch, discont = self.discont))
        else:
            raise AttributeError("Inner object does not have value_at")
        
