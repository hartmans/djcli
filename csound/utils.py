
from math import log2, ceil

class CsoundObjects:

    def __init__(self):
        self.clear()


    def clear(self):
        self.store = {}
        self.numbered = False
        self.cur_number = 0

    def append(self, item):
        if item not in self.store:
            self.store[item] = None
            self.numbered = False

    def __getitem__(self, item):
        if not item in self: self.append(item)
        if not self.numbered: self.number()
        return self.store[item]


    def __contains__(self, item):
        return item in self.store

    def __iter__(self):
        if not self.numbered: self.number()
        return iter(self.store)

    def items(self):
        if not self.numbered: self.number()
        return self.store.items()

    def number(self):
        raise NotImplementedError("Abstract method")

class CsoundTables(CsoundObjects):

    def __init__(self, base):
        super().__init__()
        self.base_table = base

    def number(self):
        cur = self.cur_number
        for k, n in self.store.items():
            if n is None:
                n = cur+self.base_table
                cur += 1
                self.store[k] = n
        self.cur_number = cur

class CsoundInstFractions(CsoundObjects):

    "Fractional instrument values used for instances of a tied note"

    def __init__(self, expected_size = 4096):
        self.denominator = 2**ceil(log2(expected_size))
        super().__init__()

    def number(self):
        if len(self.store) >= self.denominator:
            if self.cur_number == 0:
                self.denominator = 2*(2**ceil(log2(len(self.store))))
            else:
                raise RuntimeError("More than {} objects in {}, and numbering has already started".format(
                    self.denominator, self))
        cur = self.cur_number+1
        for k, n in self.store.items():
            if n is None:
                self.store[k] = cur/self.denominator
                cur += 1
        self.cur_number = cur-1
        
class CsoundTableMultiplexer:

    "Use a CsoundTables object to store multiple types of tables"

    def __init__(self, table_store, table_type):
        self.table_store = table_store
        self.table_type = table_type

    def append(self, item):
        self.table_store.append((item, self.table_type))

    def __getitem__(self, item):
        return self.table_store[(item, self.table_type)]

    def __contains__(self, item):
        return (item, self.table_type) in self.table_store
    
    def __getattr__(self, a):
        return getattr(self.table_store, a)
    
class EffectInstruments(CsoundObjects):

    def __init__(self, base, max_num):
        self.max_num = max_num
        self.base = base
        super().__init__()

    def number(self):
        cur = self.cur_number
        for k, n in self.store.items():
            if n is None:
                n = cur+self.base
                cur += 1
                self.store[k] = n
        if cur >= self.max_num:
            raise ValueError('Too many effects; raise global effects limit and adjust input orchestra')
        self.cur_number = cur
