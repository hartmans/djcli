def midi_fade(context, interaction, fader, time, val):
    if fader == -11:
        context.current_headphone_volume = val/127.0
        return
    elif fader == -12:
        context.headphone_mix = val/127.0
        return
    elif fader == -13:
        # Full on is 2 beats at 128 BPM
        context.crossfade_time = (val/127.0)*(2*60/128)
        return
    elif fader < 0:
        context.initial_fades[-fader] = val
        if context.shortcuts[-fader] and not context.shortcuts[-fader].is_placed:
            context.shortcuts[-fader].fades.initial = val
        return
    interaction.handle_midi_fade(fader, time, val)

            

def midi_map_faders(context, track):
    if track is context.shortcuts[3]:
        return 1, 14, 0
    if track is context.shortcuts[2]:
        return 2, 12, 2
    elif track is context.shortcuts[1]:
        return 1, 12, 1
    else:
        return 0,0, 0
    

def midi_jump(context, shortcut, t):
    found = context.shortcuts[shortcut]
    if found.is_placed:
        found.add_jump(t, crossfade=context.crossfade_time)
        context.mark_dirty(found)
    else: context.place(found, t, found.cue_point)
        
        
def adjust_midi(context, offset):
    context.current.adjust_jump_by(offset, time = context.time)
    context.mark_current_dirty()
    

def remove_fade(context, interaction, output, t):
    if output < 0:
        return
    interaction.handle_midi_fade(output, t, None)

def toggle_loop(context, channel, t):
    track = context.shortcuts[channel]
    try:
        track.time_of(track.loop_point)
        print('disable loop')
        track.loop_point = None
        context.mark_dirty(track)
    except:
        track.loop_point = track.offset_at(t)
        track.add_jump(t, crossfade = context.crossfade_time)
        print('Loop at {:.3f}'.format(track.loop_point))
        context.mark_dirty(track)
        


def midi_map_sliders(context, name, output):
    mapping = None
    target = (name, output)
    for s, mapping   in context.effect_sliders.items():
        if mapping == target: break
    if mapping == target:
        if s in slider_map:
            return slider_map[s]
        print("No slider bank {}".format(s))
        del context.effect_sliders[s]
    return 0, 0, 0, 0

slider_map = {
    's1': (1, 23, 22, 21),
    's2': (2, 23, 22, 21),
    }

