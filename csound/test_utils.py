from pytest import raises

from .utils import CsoundInstFractions
from fader import FaderSegment, FaderList
from .time_utils import *

def test_fractions():
    cs = CsoundInstFractions(3)
    l = [1,2,3]
    expected = [0.25, 0.5, 0.75]
    for a, b in zip(l, expected):
        assert cs[a] == b
        


def test_outer_time():
    # without discont
    assert outer_time_of(5,1,0,1) == 6
    with raises(NeverHappens):
        outer_time_of(0,1,2,1)
    assert outer_time_of(1,0,0,0.5) == 2
    discont = FaderList(0)
    discont.add(FaderSegment(1,5, 0))
    with raises(NeverHappens):
        outer_time_of(6,0,10,1)
    assert outer_time_of(6, 0, 10, 1, discont = discont) == 2
    discont.add(FaderSegment(50, 0, 0))
    assert outer_time_of(100, 1, 10, 1, discont = discont) == 150
    assert outer_time_of(40, 0, 0, 1, discont = discont) == 36
    assert outer_time_of(40, 0, 0, 1, discont = discont,
                         after = 50) == 90
    

def test_inner_time():
    assert inner_time_at(1,0, 10, 2) == 12
    discont = FaderList(0)
    discont.add(FaderSegment(5,100, 0))
    discont.add(FaderSegment(10,30,0))
    assert inner_time_at(6, 7, 0, 2, discont = discont) == -2
    assert inner_time_at(7, 0, 0, 2, discont = discont) == 104
    assert inner_time_at(1, 3, 0, 2, discont = discont) == -4
    assert inner_time_at(6, 5, 0, 2, discont = discont) == 2
    assert inner_time_at(12, 0, 0, 2, discont = discont) == 34
    
    
    

def test_faders_0dur():
    fl = FaderList(0)
    fl.add(FaderSegment(1, 3, 0))
    fl.add(FaderSegment(1,2,0))
    assert fl.value_at(1) == 2
    assert len(fl) == 1
    fl.add(FaderSegment(4,9,0))
    fl.add(FaderSegment(1,5,0))
    assert fl.value_at(1) == 5
    assert fl.value_at(4) == 9
    assert len(fl) == 2
    assert fl.list[0].start == 1
    assert fl.list[1].start == 4
    
