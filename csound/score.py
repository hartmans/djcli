from tempfile import NamedTemporaryFile


import csound.time_utils


from .utils import CsoundInstFractions, CsoundTables, CsoundTableMultiplexer, EffectInstruments
from fader import FaderList
from .midi import midi_map_faders, midi_map_sliders

csound_tables = CsoundTables(50)

sound_tables = CsoundTableMultiplexer(csound_tables, 'sound_file')
fader_tables = CsoundTableMultiplexer(csound_tables, 'faders')
table_callbacks = dict()
instr_fractions = CsoundInstFractions()
track_outputs = None

loaded_songs = []

class LoopAdapter(csound.time_utils.TimeAdapter):

    def __init__(self, track, crossfade_time):
        super().__init__(track.jumps,
                         start = track.start,
                         cue_point = track.cue_point,
                         pitch = 1.0)
        self.loop_point = track.loop_point
        self.jump_point = track.jump_point
        self.track = track
        self.crossfade_time = crossfade_time

    def after_time(self, t):
        after = t
        for s, v, d in  super().after_time(t):
            yield s, v, d
            after = max(s, after)
        if self.loop_point is None: return
        try:
            if self.track.time_of(self.loop_point, after = after) > after:
                yield self.track.time_of(self.loop_point, after = after), self.jump_point, self.crossfade_time
        except  csound.time_utils.NeverHappens: pass

class CsoundScore:

    def __init__(self, base_time, orchestra, context):
        global track_outputs
        self.f = NamedTemporaryFile('wt')
        for m in ('write', 'close', 'flush', 'name'):
            setattr(self,m, getattr(self.f,m))
        self.interaction = CsoundInteraction(self.f, base_time, orchestra = orchestra, context = context, score_time = True)
        csound_tables.clear()
        table_callbacks.clear()
        instr_fractions.clear()
        loaded_songs.clear()
        track_outputs = EffectInstruments(1, orchestra.outputs-1)
        self.orchestra = orchestra
        self.context = context



    def __getattr__(self, a):
        return getattr(self.interaction, a)

    def realtime_interaction(self, csound_in):
        rt =  RealtimeCsoundInteraction(QueueIoWrapper(csound_in), orchestra = self.orchestra, base_time = self.base_time, context = self.context)
        rt.midi_slider_callbacks = self.midi_slider_callbacks
        return rt


class CsoundInteraction:

    def __init__(self, f, base_time, orchestra, context, score_time = False):
        self.f = f
        self.base_time = base_time
        self.orchestra = orchestra
        self.context = context
        self.do_score_time = score_time
        self.midi_slider_callbacks = {}


    def score_time(self, t):
        "For realtime mode, return 0 and trust the instrument or function table to handle appropriately; for score generation, return the time"
        if self.do_score_time:
            if t > self.base_time: return t-self.base_time
            else: return 0
        else: return 0


    def sound_file(self, playing_song):
        song  = playing_song.song
        if song in loaded_songs: return
        sound_tables.append(song)
        if self.song_will_play(playing_song):
            self.f.write(
                '''f {tb} {t} 0 -1 "{f}" 0 0 0
'''\
                .format(tb = sound_tables[song], t = self.score_time(playing_song.start),
                        f = song.wav))
            loaded_songs.append(song)


    def handle_context_sound_files(self, context):
        for  t in context.all_tracks:
            sound_tables.append(t.song)
        for t in context.all_tracks:
            self.sound_file(t)

    def sound_table(self, playing_song):
        assert playing_song.song in sound_tables
        return sound_tables[playing_song.song]

    def score_fader_list(self, fl, *, since = None):
        if since is None: since = self.base_time
        if since > self.base_time: since = self.base_time
        initial = fl.value_at(since)
        l = [initial]
        for t, v, d in fl.after_time(since):
            l.extend([t,  v, d])
        l = [ format(x, ".4f") for x in l]
        if len(l) > 900:
            fl.file = NamedTemporaryFile("wt")
            fl.file.write("\n".join(l))
            fl.file.write("\n")
            fl.file.flush()
            self.f.write(
                '''f {tb} {t} 0 -23 "{n}"
'''.format(
                tb = fader_tables[fl.inner],
                t = 0, # Avoid fader being overwritten
                l = len(l),
                n = fl.file.name))
            fl.inner.file = fl.file
        else:
            self.f.write(
                '''f {tb} {t} -{l} -2 {items}
'''.format(
                tb = fader_tables[fl.inner],
                t = 0, # Avoid fader being overwritten
                l = len(l),
                items = " ".join(l)))

    def score_effects(self, effects, output, midi_cb):
        if output == 0:
            instruments = self.orchestra.master_instruments
        else: instruments = self.orchestra.bus_instruments
        for start, e, dur in effects.after_time(self.base_time):
            end = start+dur
            if e.effect not in instruments:
                print("Unable to render {}: restart csound".format(e.effect.name))
                continue
            self.f.write('''
i {instantiate} {t} -1 {inum} {start} {end} {output} "{args}"
            '''.format(
                instantiate = 59+instr_fractions[e]  if (output == 0)  else 11+instr_fractions[e],
                t = self.score_time(start),
                inum = instruments[e.effect]+instr_fractions[e],
                start = start,
                end = end,
                dur = dur,
                output = output,
                args = e.args))
            mapping = midi_map_sliders(self.context, e.effect.name, output)
            mapping = " ".join(str(m) for m in mapping)
            tables = self.score_effect_sliders(output, effects.inner, effects.start, effects.cue_point, effects.pitch, e.effect, midi_cb)
            self.f.write('''
i {inst_fraction} {t} -1 {start} {end} {output} {base} {tables} {mapping}\n'''
                         .format(
                             inst_fraction = 57+instr_fractions[e],
                             t = self.score_time(start),
                             output = output,
                             start = start,
                             end = end,
                             base = e.effect.slider_base,
                             tables = " ".join([str(t) for t in tables]),
                             mapping = mapping
                             ))



    def score_effect_sliders(self, output, effect_list, start, cue_point, pitch, effect, midi_cb):
        effect_sliders = effect_list[effect.name]
        res = []
        for i, s in enumerate(effect_sliders):
            if (s is None):
                res.append(0)
                continue
            assert len(res) == i
            self.midi_slider_callbacks[effect.zk(output, i)] = midi_cb(effect.name, i)
            self.score_fader_list(csound.time_utils.TimeAdapter(s,
                                                                start, cue_point, 1.0))
            res.append(fader_tables[s])
        return res[1:]




    def score_track(self, track):
        def midi_effect_cb(effect, slider):
            def handle_slide(t, val):
                if val is None:
                    track.remove_slides(effect, slider, t, t+0.05)
                else:
                    if not track.is_placed:
                        t = "initial"
                    track.add_slide(effect, slider, t, val, 0.01)
            return handle_slide
        def handle_midi_fade(t, val):
            if (self.context.preview is track ) or (track.start is None):
                self.context.place(track, t, track.offset_at(t))
            else:
                if val is None:
                    track.remove_fades(t, t+0.05)
                else:
                    track.add_fade(t,val, 0.01)

        def end_of_jumps_cb():
            if track.loop_point is not None:
                track.add_jump(track.time_of(track.loop_point, track_after = track.last_jump_track_time),
                               crossfade = self.context.crossfade_time)
                self.context.mark_dirty(track)
        track.output = track_outputs[track]
        if track.start is None or track.mute: return self.silence_track(track)
        if not self.song_will_play(track): return self.silence_track(track)
        chn, ctrl, cross = midi_map_faders(self.context, track)
        if self.context.preview is track:
            fades = FaderList(1, 0)
        else:
            fades = track.fades
        jump_adapter =LoopAdapter(track, self.context.crossfade_time)
        try: since = track.time_of_track(track.jump_before_track_time(self.base_time)-0.01)
        except csound.time_utils.NeverHappens: since = 0
        self.score_fader_list(jump_adapter, since = since)
        try: track.calc_last_time()
        except csound.time_utils.NeverHappens: return
        table_callbacks[fader_tables[track.jumps]] = end_of_jumps_cb
        score = '''
i {instr} {time} -1 "{f}" {output} {volume} {start} {cue_point} {end} {pitch} {jump} {jump_table} {transpose}
f 0 {end}
        '''.format(
            f = track.song.wav,
            output = track.output,
            pitch = track.scale,
            start = track.start,
            cue_point = track.cue_point,
            jump = track.jump_point,
            end = track.calc_last_time(ignore_fadeout = track is self.context.current),
            instr = 10+instr_fractions[track],
            time = self.score_time(track.start),
            jump_table = fader_tables[track.jumps],
            volume = track.volume,
            transpose = track.transpose)
        fades_adapter = csound.time_utils.TimeAdapter(fades,
                                        start = track.start,
                                        cue_point = track.cue_point,
                                                      pitch = 1.0)
        self.score_fader_list(fades_adapter)
        self.midi_slider_callbacks[track.output] = handle_midi_fade
        score = score+ \
                '''i {instr} {t} -1 {output} {start} {end} {ifn} {chn} {ctrl} {cross}
'''\
                    .format(
                        t = self.score_time(track.start),
                        instr = 58+instr_fractions[track],
                    output = track.output,
                    start = track.start,
                    ifn = fader_tables[fades],
                        chn = chn,
                        ctrl = ctrl,
                        cross = cross,
                        end = track.calc_last_time(ignore_fadeout = track is self.context.current))


        self.f.write(score)
        if track.effects_enabled:
            effects = csound.time_utils.TimeAdapter(track.effects, track.start, track.cue_point, 1.0)
            self.score_effects(effects, track.output, midi_effect_cb)

    def score_globals(self):
        context = self.context
        try: current_output = context.current.output
        except: current_output = None
        try: other_output = context.shortcuts[context.other_fader_idx].output
        except: other_output = 0
        self.f.write("i \"faders\" 0 0.1 {} {} {} {headphonecurrent} {headphonemix} {wheels}\n".format(
            int( context.enable_faders),
            current_output, other_output,
            headphonecurrent = context.current_headphone_volume,
            headphonemix = context.headphone_mix,
            wheels = 1 if context.current is context.preview else 0
        ))

    def score_context(self, context):
        for t in context.all_tracks:
            if context.solo and context.solo is not t: continue
            if t is context.preview and not context.preview_playing: continue
            self.score_track(t)
        if context.preview and context.preview not in context.all_tracks and context.preview_playing:
            self.score_track(context.preview)
        if context.metronome:
            self.f.write('i 12 0 -1 {}\n'.format(context.metronome))
        self.score_global_effects()
        self.score_globals()

    def score_global_effects(self):
        context = self.context
        def global_effects_callback(effect, slider):
            def handle_slide(t, val):
                from fader import FaderSegment
                if val is None:
                    self.context.effects[effect][slider].remove_between(t, t+0.05)
                else:
                    self.context.effects[effect][slider].add(FaderSegment(t, val, 0.01))

            return handle_slide
        effects = csound.time_utils.TimeAdapter(context.effects, 0, 0, 1.0)
        self.score_effects(effects, 0, global_effects_callback)

    def silence_track(self, track):
        pass #Only for realtime


    def song_will_play(self, playing_song):
        if playing_song.start > self.base_time: return True
        if playing_song.calc_last_time(ignore_fadeout = playing_song is self.context.current) > self.base_time: return True
        return False


class RealtimeCsoundInteraction(CsoundInteraction):

    def __init__(self, f, base_time, orchestra, context):
        super().__init__(f, base_time, orchestra = orchestra, score_time = False, context = context)

    def update_time(self, t, score = False):
        self.base_time = t
        if score:
            self.f.write("i \"timechange\" 0 0.1 {}\n".format(t))


    def silence_track(self, track):
        self.f.write("f {tb} {t} -1 -2 0\n".format(
            tb = fader_tables[track.fades],
            t = 0))
        self.f.write("i {inum} {t} 0\n".format(
            inum = -(10+instr_fractions[track]),
            t = self.score_time(track.start+0.1 if track.start else 0)))
        self.f.write("i -{instr} {t} 0\n".format(
        instr = 58+instr_fractions[track],
t = self.score_time(track.start+0.01 if track.start else 0)))

    def empty_table(self, tn):
        if tn in table_callbacks:

            table_callbacks[tn]()
    def play_excerpt(self, track, pos):
        score = '''
i {instr} 0 -1 "{f}" {output} {volume} {start} {cue_point} {end} {pitch} 0 0 {transpose}
        '''.format(
            f = track.song.wav,
            output = self.orchestra.outputs-1,
            end = self.context.time+2,
            pitch = track.scale,
            start = self.context.time,
            cue_point = pos,
            instr = 10+instr_fractions['excerpt'],
            volume = track.volume*1.2,
            transpose = track.transpose)
        self.f.write(score)

    def handle_midi_fade(self, table, time, val):
        if table in self.midi_slider_callbacks:
            self.midi_slider_callbacks[table](time, val)
            

class QueueIoWrapper:

    def __init__(self, q):
        self.q = q

    def write(self, s):
        self.q.put(s)

    def flush(self):
        pass

