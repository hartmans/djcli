import sh, asyncio, os, sys, time, queue
from tempfile import NamedTemporaryFile
from context import context
from fader import FaderSegment

from .score import CsoundScore, RealtimeCsoundInteraction
from .orc import CsoundOrchestra
from .midi import midi_fade, midi_jump
from . import midi
from . import jack

CSOUND_LIVE_INFINITY = 10000

csound_in = queue.Queue(10)
csound_cmd = None
orc_file = None
csound_output = ""
dac = "-odac"
csound_interaction = None

def  out_callback(data):
    global csound_output
    try:
        if not isinstance(data, str):
            data = str(data, 'utf-8')
        command, sep, tail = data.partition(' ')
        if command == 'time':
            if context.now is not None:
                context.time = float(tail)
                context.now = time.time()
                if csound_interaction is not None:
                    csound_interaction.update_time(context.time)
                if context.global_loop is not None and context.time > context.global_loop[1]:
                    context.time = context.global_loop[0]
                    if csound_interaction: csound_interaction.update_time(context.time, True)
        elif command == 'f2':
            t, tab, v = tail.split()
            t = float(t)
            tab = int(tab)
            v = int(v)
            midi_fade(context, csound_interaction, tab, t, v)
        elif command  == 'rfd':
            o, t = tail.split()
            o = int(o)
            t = float(t)
            midi.remove_fade(context, csound_interaction, o, t)
        elif command == 'empty_table':
            if csound_interaction is not None:
                csound_interaction.empty_table(int(tail))
        elif command == 'adjust':
            midi.adjust_midi(context, float(tail))
        elif command == 'pitch':
            context.current.scale = float(tail)
        elif command == 'offset':
            cue_time, sep, cue_point = tail.partition(' ')
            context.current.start = float(cue_time)
            context.current.jump_point = float(cue_point)
        elif command == 'jump':
            o, t = tail.split()
            midi_jump(context, int(o), float(t))
        elif command == 'toggle_loop':
            channel, t = tail.split()
            midi.toggle_loop(context, int(channel), float(t))
        elif command == 'cmd':
            tail = tail.rstrip('\n')
            cb_execute_command(tail)
        else:
            csound_output += data
    except  Exception as e:
        print(str(e))
        if context.debug: raise 
        

    
async def run_csound(context, execute_command, config, output = None):
    global cb_execute_command
    cb_execute_command = execute_command # store away the eexecute_command so the callback can call it
    # Yes, we have too many globals
    if output is None:
        output = dac
    global csound_cmd
    global csound_in
    global score_file
    global orc_file
    global csound_output
    global csound_interaction
    csound_output = ""
    new_orc_file = CsoundOrchestra(200, context)
    new_score_file = CsoundScore(context.time, orchestra = new_orc_file,
                                 context = context)
    will_score = False
    for p in context.all_tracks:
        if context.solo and context.solo is not p: continue
        if p and new_score_file.song_will_play(p): will_score = True
    if context.preview and new_score_file.song_will_play(context.preview):
        will_score = True
    if config.live: will_score = True
    if will_score:
        new_orc_file.gen_orc_file(context.time)
        new_score_file.score_context(context)
        if csound_cmd is not None:
            terminate_csound()
            score_file.close()
            orc_file.close()
        score_file = new_score_file
        orc_file = new_orc_file
        if config.live:
            score_file.write('''f 0 10000
i 3 0 -1 -1 127 1 12 1
i 3 0 -1 -2 127 2 12 2
''')

                
            
        score_file.flush()
        context.rescore = False
        context.start_time = context.time
        try:
            csound_interaction = score_file.realtime_interaction(csound_in)
        except Exception as e:
            print(e)
            sys.exit(2)
        csound_args = []
        if config.midi_device:
            csound_args.append('--midi-device='+config.midi_device)
        if config.adc is not None and config.adc != "":
            csound_args.append("-iadc"+str(config.adc))
        if config.channels_4:
            csound_args.append('--nchnls=4')
        if config.jack:
            bufsize, sr = jack.jack_info()

            csound_args.extend(['-+rtaudio=jack', 
            '-+rtmidi=jack',
'-r{}'.format(sr),
                                '--ksmps={}'.format(bufsize),
                                '-b{}'.format(bufsize),
                                '-B{}'.format(bufsize*2),
                                '--sched=20', '-d'])
        csound_cmd =sh.csound(output,
                          '-+rtmidi=alsaseq',
#                          "-+raw_midi=yes"
                          "-b64", "-B128", "-+rtaudio=pulse",
                          '-Lstdin',
                          '-+msg_color=false',
                          *csound_args,
                          orc_file.name, score_file.name, _bg = True,
                          _bg_exc = False,
                                                    _err = out_callback,
                          _in = csound_in,
                          _done = csound_done)
        

def csound_done(command, success, exit_code):
    global csound_interaction
    csound_interaction = None
    csound_in = queue.Queue(10)
    context.preview_playing = False
    
    
    
def terminate_csound():
    global csound_cmd
    if csound_cmd is not None:
        if csound_cmd.process.is_alive()[0]: 
            csound_in.put("e\n")
            csound_cmd.kill()
            try:
                csound_cmd.wait()
            except: pass
        csound_cmd = None
        
