from ui.interface import *

class SoloCommand(Command):

    name = 'solo'
    aliases = ('s+', 'sol')

    def execute(self, arg, context):
        context.solo = context.current
        context.rescore = True

command_list.add_command(SoloCommand)

class NoSoloCommand(Command):

    name = 'no_solo'
    aliases = ('nosolo', 's-')

    def execute(self, arg, context):
        context.solo = None
        context.rescore = True

command_list.add_command(NoSoloCommand)

class MuteCommand(Command):

    name ='mute'
    aliases=('m+',)

    def execute(self, arg, context):
        context.current.mute = True
        context.rescore = True

command_list.add_command(MuteCommand)

class NoMuteCommand(Command):

    name = 'no_mute'
    aliases = ('m-',)

    def execute(self, arg, context):
        context.current.mute = False
        context.rescore = True

command_list.add_command(NoMuteCommand)
