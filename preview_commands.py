
from ui.interface import *

class PreviewCommand(Command):

    name = 'preview'

    def execute(self, arg, context):
        if context.preview is context.current:
            context.preview_playing = not context.preview_playing #toggle whether preview is playing
        else:
            context.preview = context.current
            context.preview_playing = True
            context.mark_silent(context.current)
        context.mark_current_dirty(restart_preview = True)
        
command_list.add_command(PreviewCommand)


class HeadphoneSetting1(Command):

    name = '1'

    def execute(self, arg, context):
        context.current_headphone_volume = 1.0
        context.headphone_mix = 0
        context.mark_globals_dirty()
        

command_list.add_command(HeadphoneSetting1)

class HeadphoneSetting2(Command):

    name = '2'

    def execute(self, arg, context):
        context.current_headphone_volume = 0.0
        context.headphone_mix = 1.0
        context.mark_globals_dirty()
        

command_list.add_command(HeadphoneSetting2)

class HeadphoneSetting3(Command):

    name = '3'

    def execute(self, arg, context):
        context.current_headphone_volume = 1.0
        context.headphone_mix = 1.0
        context.mark_globals_dirty()
        

command_list.add_command(HeadphoneSetting3)

class HeadphoneSetting4(Command):

    name = '4'

    def execute(self, arg, context):
        context.current_headphone_volume = 0.5
        context.headphone_mix = 0.7
        context.mark_globals_dirty()
        

command_list.add_command(HeadphoneSetting4)

class HeadphoneSetting5(Command):

    name = '5'

    def execute(self, arg, context):
        context.current_headphone_volume = 0.7
        context.headphone_mix = 0.5
        context.mark_globals_dirty()
        
command_list.add_command(HeadphoneSetting5)

class HeadphoneSetting0(Command):

    name = '0'

    def execute(self, arg, context):
        context.current_headphone_volume = 0.0
        context.headphone_mix = 0.2
        context.mark_globals_dirty()
        

command_list.add_command(HeadphoneSetting0)



class PlaceCommand(Command):

    name = 'place'

    arguments = ListArgument(NumberArgument, TimeArgument)

    def execute(self, arg, context):
        t, cue = arg
        if not t: t = context.time
        if not cue: cue = context.current.offset_at(t)
        context.place(context.current, t, cue)
command_list.add_command(PlaceCommand)
