#!/usr/bin/python
import contextlib

from sqlalchemy import Column, String, ForeignKey, UniqueConstraint, Integer, Float, PrimaryKeyConstraint, create_engine
from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
import os.path
from ui.interface import CompletionHelper
import re

Base = declarative_base()
class CuePointHeader(Base):
    __tablename__ = 'song_entry'
    id = Column(Integer, primary_key = True)
    title = Column(String(200), nullable = False)
    artist = Column(String(200))
    album = Column(String(200))
    file_name = Column(String(400))
    bpm = Column(Float)
    
    __table_args__ = (UniqueConstraint(title, artist, album),)

    @classmethod
    def get_or_create(cls, metadata, session):
        inst = session.query(cls).filter_by(**metadata).first()
        if inst: return inst
        inst = cls(**metadata)
        session.add(inst)
        return inst

    @property
    def max_cue_point(self):
        return len(self.cue_points)+1

    def update_cue_point(self, number, t):
        with session_wrap() as s:
            s.add(self)
            if number :
                try: del self.cue_points[number-1]
                except IndexError: pass
            for cp in self.cue_points:
                if abs(cp.t-t) < 0.1: s.delete(cp)
            self.cue_points.append(CuePoint(t = t))

    def delete_cue_point(self, number):
        with session_wrap() as s:
            s.add(self)
            try: del self.cue_points[number-1]
            except IndexError:
                raise IndexError('cue_point not found') from None

    def update(self, **kwargs):
        with session_wrap() as s:
            s.add(self)
            for k in kwargs:
                setattr(self, k, kwargs[k])

            
    def cue_point(self, number):
        return self.cue_points[number-1]
                               
class CuePoint(Base):
    __tablename__ = 'cue_points'
    song_id = Column(Integer, ForeignKey(CuePointHeader.id), nullable = False)
    t = Column(Float, nullable=False)

    
    __table_args__ = (PrimaryKeyConstraint(song_id, t), )
    song = relationship(CuePointHeader, lazy='subquery',
                        backref = backref('cue_points', uselist = True, lazy = 'joined',
                                          cascade ='all, delete-orphan',
                                          order_by = [song_id, t]))
    __mapper_args__ = {'order_by': [song_id, t]}
    

    
sessions = sessionmaker()

def setup(path = "~/.djcli.cue_points"):
    uri = 'sqlite:////'+os.path.expanduser(path)
    e = create_engine(uri)
    sessions.configure(bind = e)
    Base.metadata.create_all(e)
    

@contextlib.contextmanager
def session_wrap():
    s = sessions()
    yield s
    s.commit()
    for o in s: s.refresh(o)
    s.close()

class CuedSongCompleter(CompletionHelper):

    def __init__(self):
        super().__init__(min_length = 6)

    def completion_tuples(self):
        with session_wrap() as s:
            songs = s.query(CuePointHeader).filter(
                CuePointHeader.file_name != None).all()
        for s in songs:
            yield self.song_desc(s).lower(), s.file_name, self.song_desc(s)
            

    def parse(self, tail):
        return tail

    def match_re(self, match_text):
        match_text = re.escape(match_text.lower())
        match_text = re.sub(r'\\ ', '.+', match_text)
        return re.compile(match_text)

    def song_desc(self, s):
        desc = "{a}_{t}".format(a = s.artist,
                                t = s.title)
        if s.bpm:
            desc = "{:.1f}:".format(s.bpm)+desc
        return desc
