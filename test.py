#!/usr/bin/python3
import asyncio, tempfile
import sh

from songutils import Song, match_songs
import sys
async def run():
    s1 =  Song(sys.argv[1])
    s2 =  Song(sys.argv[2])
    s1, s2 = await asyncio.gather(s1, s2)
    time_mixin = float(sys.argv[3])
    time_offset = float(sys.argv[4])
    t1, t2, scale = match_songs(s1, s2, time_mixin, time_offset)
    #At time_mixin we start song 2 such that by t1, we are playing at
    #point t2 in song 2.  Naturally, t1 must be greater than
    #time_mixin.  So we reduce t2 by t1-time_mixin.
    t2 = ((time_mixin-t1)/scale)+t2
    score = '''
i 1 0 90.0 0.6 "{s1}" 1.0 0.0
i2 {tmix} 90.0 0.6 "{s2}" {scale} {t2}
'''.format(
    s1 = s1.wav,
    s2 = s2.wav,
    tmix = time_mixin,
    t2 = t2,
    scale = scale)
    with tempfile.NamedTemporaryFile('wt', encoding = 'utf-8') as f:
        f.write(score)
        f.flush()
        sh.csound('-odac', 'mix.orc', f.name,
                  _fg = True)
        
                     
    

loop = asyncio.get_event_loop()
loop.run_until_complete(run())
