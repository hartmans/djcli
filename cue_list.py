from context import context
import csound
import asyncio, os.path, traceback, sh

class CueList:

    def __init__(self, tracks):
        self.tracks  = sorted(tracks, key = lambda t: t.start)

    def print_markdown(self, fn):
        with open(fn, "wt", encoding = 'utf-8') as f:
            for t in self.tracks:
                f.write("## {t1:.1f}–{t2:.1f}: {title}\n".format(
                    t1 = t.start,
                    t2 = t.last_time,
                    title = t.name))

    def print_cue_sheet(self, fname):
        def safe(f):
            try:
                s = f()
                return s.replace('"', '\\"')
            except: return ""
            
        with open(fname, "wt", encoding = 'utf-8') as f:
            f.write('''
TITLE "A Mix"
PERFORMER "Performer"
FILE "foo.mp3" MP3
''')
            tracknum = 1
            start_time = 0
            for t,  track in  self._break_tracks():
                f.write('''
  TRACK {tn:02} AUDIO
    TITLE "{title}"
    PERFORMER "{artist}"
    INDEX 01 {start_time}
    INDEX 00 {start_time}
'''\
                      .format(
                          tn = tracknum,
                          title = safe(lambda : track.song.metadata['title']),
                          artist = safe(lambda: track.song.metadata['artist']),
                          start_time = self._hmf(start_time)))
                start_time = t
                tracknum += 1
                

    def _break_tracks(self):
        last_ending = -1
        tracks = []
        # We find the break points between tracks
        # so metadata (and name) comes from the last track not the current track
        this_track = self.tracks[0]
        t = 0
        for trk in self.tracks:
            break_time = trk.break_time(last_ending)
            last_ending = trk.last_time
            if break_time is None: continue
            duration = break_time-t
            if duration < 0.5: continue
            t = break_time
            yield t, this_track
            this_track = trk
        if t > 0:
            yield t, this_track

    async def split(self, context, format = 'mp3'):
        dir = "/tmp/split/"
        context.time = 0
        try: csound.terminate_csound()
        except: pass
        if os.path.exists(dir+"output.wav") :
            print("Using cached output; delete split directory to re-render")
        else:
            await csound.run_csound(context, "-o{}".format(
                dir+"output.wav"))
            await csound.csound_cmd
        sox_cmd = ""
        t = 0
        last_ending = -1
        tracks = []
        # We find the break points between tracks
        # so metadata (and name) comes from the last track not the current track
        this_track = self.tracks[0]
        for trk in self.tracks:
            break_time = trk.break_time(last_ending)
            last_ending = trk.last_time
            if break_time is None: continue
            duration = break_time-t
            if duration < 0.5: continue
            t = break_time
            sox_cmd += "trim  0 {dur} : newfile : ".format(
                dur = duration)
            tracks.append(this_track)
            this_track = trk

        tracks.append(this_track)
        sh.sox(dir+"output.wav",
               dir+"track%n.wav",
               *sox_cmd.split())
        async def handle_mp3(track, num):
            print("Tagging {}".format(track.name))
            encoded_track =                       "{}{}.mp3".format(
                dir, track.filename_base(num))
            os.rename("{}track{:02}.mp3".format(
                dir, num),
                      encoded_track)
            metadata = self._get_metadata(context, track, num, len(tracks))
            id3v2_args = []
            for k,v in id3v2_map.items():
                if k in metadata:
                    id3v2_args.append('--{}={}'.format(
                        v, metadata[k]))
            sh.id3v2(*id3v2_args, encoded_track)
            print("Done tagging {}".format(track.name))

        if  format == 'mp3':
            # We need to do a single pass encode of the tracks to permit gapless playback
            print("Performing single pass mp3 encode")
            sh.lame('-V2', '-b324',
                    '--nogapout', dir,
                    '--nogap',
                    *("{}track{:02}.wav".format(dir, x) for x in range(1, len(tracks)+1))
                    )
            for num, track in enumerate(tracks, 1):
                encode_queue.put_nowait(handle_mp3(track, num))
        else:
                raise ValueError('only mp3 supported for now')
            

    def _get_metadata(self, context, track, num, total):
        metadata = dict(track.song.metadata)
        metadata['track'] = "{}/{}".format(num,total)
        for k in ('album', 'remixed_by'):
            if not hasattr(context, k): continue
            if k in metadata: metadata['orig_'+k] = metadata[k]
            metadata[k] = getattr(context, k)
        return metadata

    def _hmf(self, t):
        minutes = int(t//60)
        seconds = int(t%60)
        return "{mm:02d}:{ss:02d}:00".format(
            mm = minutes,
            ss = seconds)
    
encode_queue = asyncio.Queue()

async def queue_worker():
    while True:
        item = await encode_queue.get()
        try:
            await item
        except Exception:
            traceback.print_exc()

for i in range(2):
    asyncio.get_event_loop().create_task(queue_worker())

id3v2_map = {
    'album': 'TALB',
    'orig_album': 'TOAL',
    'title': 'TIT2',
    'year': 'TYER',
    'artist': 'TPE1',
    'remixed_by': 'TPE4',
'track': 'TRCK'
    }

__all__ = ['CueList']

