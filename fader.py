from collections import namedtuple
from functools import total_ordering
from yaml import YAMLObject
from bisect import bisect_left
from copy import copy


FaderSegment_base = namedtuple('FaderSegment', ('start','val','dur'))

@total_ordering
class FaderSegment(FaderSegment_base):
    def __lt__(self, other):
# Note that a pure number sorts directly after a segment with that given start
        # This means that if there is a zero duration segment, value_at will return the right answer.
        if isinstance(other,(float,int)): return self.start <= other
        if self.start < other.start: return True
        if self.start == other.start: return self.dur < other.dur
        return False

    def __eq__(self, other):
        if not isinstance(other,FaderSegment): return False
        return (self.start == other.start) and (self.dur == other.dur) and (self.val == other.val)
    @property
    def end(self):
        return self.start+self.dur


class FaderList(YAMLObject):

    yaml_tag = 'fader_list'

    def __init__(self, fader, initial = None):
        self.list = []
        self.fader = fader
        if initial is None:
            self.initial = 127 if fader == 1 else 0
        else: self.initial = initial

    def add(self, seg):
        index = bisect_left(self.list, seg)
        if index == len(self.list):
            self.list.insert(index,seg)
        else:
            other  = self.list[index]
            right_overlap = seg.end-other.start
            if right_overlap >= 0:
                other = FaderSegment(seg.end, other.val, other.dur-right_overlap)
                if other.dur >0:
                    self.list[index] = other
                else:
                    self.list.pop(index)
            self.list.insert(index,seg)
        if index > 0:
            prev = self.list[index-1]
            left_overlap = prev.end-seg.start
            if left_overlap >= 0:
                prev = FaderSegment(prev.start, prev.val, prev.dur-left_overlap)
                if prev.dur> 0:
                    self.list[index-1] = prev
                else: self.list.pop(index-1)
                
    def remove_between(self, beg, end):
        i = bisect_left(self.list,beg)
        if i > 0: #Since pure numbers sort after segments we may have missed a boundary
            i = i-1
        while i < len(self.list):
            elt = self.list[i]
            if (beg <= elt.start <= end) or ( beg < elt.end < end):
                self.list.pop(i)
            else: i += 1

            
    def value_at(self, pos):
        '''
        :param pos:
            A position or segment
        :return:The value of the function at the given position.
        '''
        index = bisect_left(self.list,pos)
        if (index == 0) or (len(self.list) ==0):
            return self.initial
        prev_seg = self.list[index-1]
        if pos >= prev_seg.end: return prev_seg.val
        #a is val at prev_seg.start
        if index == 1: a = self.initial
        else: a = self.list[index-2].val
        slope = (prev_seg.val-a)/prev_seg.dur
        return a+slope*(pos-prev_seg.start)

    def value_sloped(self, pos, slope):
        "generally intended for FaderLists used for time discontinuities where the durations ignored. Draw a series of line segments all of specified slope, discontinuous at each new segment.  Find the value of the function at a given position ignoring duration."
        index = bisect_left(self.list,pos)
        if (index == 0) or (len(self.list) ==0):
            return self.initial+slope*pos
        prev_seg = self.list[index-1]
        delta = pos - prev_seg.end
        return prev_seg.val + delta*slope

    def segment_before(self, pos):
        '''
        :returns: The segment that has started most recently before *pos*.
        '''
        index = bisect_left(self.list,pos)
        if (index == 0) or (len(self.list) ==0):
            return None
        prev_seg = self.list[index-1]
        return prev_seg
        
        

    def to_score(self, offset, start, scale):
        index = bisect_left(self.list, offset)
        val = self.value_at(offset)
        score = "i30  {start} 0.01 {f} {val}\n".format(
            f = self.fader,start = start,
            val = val)
        for elt in self.list[index:]:
            if offset > elt.end: continue
            before_time = max(0,offset-elt.start)
            score += "i 30 {t} {d} {f} {v}\n".format(
                f = self.fader,
                t = start+max(0, (elt.start - offset)/scale),
                d = (elt.dur/scale)-before_time,
                v = elt.val)

        return score

    def after_time(self, offset):
        index = bisect_left(self.list, offset)
        if index > 0:
            elt = self.list[index-1]
            if offset <  elt.end: yield elt.start, elt.val, elt.dur
        for e in self.list[index:]:
            yield e.start, e.val, e.dur
            


    def __getstate__(self):
        l = list(map( lambda x: [x.start, x.val, x.dur], self.list))
        return {'l': l,
                'initial': self.initial,
                'fader': self.fader}

    def __setstate__(self, state):
        self.list = list(map(lambda x: FaderSegment(*x), state['l']))
        self.initial = state['initial']
        self.fader = state['fader']

    def copy(self):
        new = self.__class__(self.fader, initial = self.initial)
        new.list = copy(self.list)
        return new
    
    def __len__(self):
        return len(self.list)

    def pitch_scale(self, pitch):
        "Mostly used for format conversions"
        new = FaderList(self.fader, initial = self.initial)
        for seg in self.list:
            new.add(FaderSegment(seg.start*pitch, seg.val, seg.dur*pitch))
        return new
    

